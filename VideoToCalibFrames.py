import cv2
import numpy as np
import glob

# Parameter des Schachbrett
chessboard_size = (6, 4)
# Anzahl der inneren Ecken in beiden Richtungen
square_size = 0.0355  # meter
# 3D-Punkte in der Weltkoordinateneben
objp = np.zeros((chessboard_size[0] * chessboard_size[1], 3), np.float32)
objp[:, :2] = np.mgrid[0:chessboard_size[0], 0:chessboard_size[1]].T.reshape(-1, 2)
objp *= square_size
# Arrays zum Speichern der 3D-Punkte und der 2D-Bildpunkte
objpoints = []  # 3D-Punkte im Weltkoordinatensystem
imgpoints = []  # 2D-Punkte im Bildkoordinatensystem

# Videoquelle öffnen (Webcam)
cap = cv2.VideoCapture("chess6.mp4")  # '0' für die erste Webcam
count = 0
skip_frames = 25
while True:

    for i in range(0, skip_frames):
        ret, frame = cap.read()
    if not ret:
        break

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Schachbrett-Ecken finden
    ret, corners = cv2.findChessboardCorners(gray, chessboard_size, None)

    if ret:
        # Ecken auf dem Frame zeichnen
        frame2 = cv2.drawChessboardCorners(frame, chessboard_size, corners, ret)
        # Zeige das Live-Video mit den gezeichneten Ecken
        cv2.imwrite(f'calibFrames/{count:03d}chess.png', frame2)
        cv2.imwrite(f'calibFrames/{count:03d}orig.png', gray)

    count += 1

cap.release()
