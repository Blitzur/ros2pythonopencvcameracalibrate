# open CV stuff
import cv2
import numpy as np
import time
import torch
from ultralytics import YOLO
import matplotlib.pyplot as plt

# Load the YOLO model
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = YOLO('yolov8n-pose.pt')

undistortMode = False

# Open the video source (0 for webcam, or provide a video file path)
video_source1 = "human4.mp4"  # "/dev/video4"
video_source2 = "human5.mp4"  # "/dev/video10"
cap1 = cv2.VideoCapture(video_source1)
cap2 = cv2.VideoCapture(video_source2)

# Check if video source opened successfully
if not cap1.isOpened() or not cap2.isOpened():
    print("Error: Could not open video source.")
    exit()

# Set desired FPS
desired_fps = 30
frame_time = 1.0 / desired_fps

# origin points. 3 points that ALL cameras can see
undist4originpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
undist5originpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
undist6originpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
undist7originpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo

distorigin4 = np.array([[1088, 795], [1057, 776], [1006, 816]])  # todo check if box in the middle?
distorigin5 = np.array([[1047, 830], [1023, 851], [1006, 816]])  # todo check if box in the middle?
distorigin6 = np.array([[0, 0], [0, 0], [0, 0]])  # todo
distorigin7 = np.array([[0, 0], [0, 0], [0, 0]])  # todo

dist4_45_refpoints = np.array([[1500, 566], [1465, 556], [1419, 541], [1290, 513], [1258, 502], [1299, 425]],
                              dtype=np.float32)
dist5_45_refpoints = np.array([[865, 585], [836, 592], [798, 600], [690, 631], [642, 639], [561, 561]],
                              dtype=np.float32)

dist5_56_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
dist6_56_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo

dist6_67_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
dist7_67_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo

dist7_74_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
dist4_74_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo

dist6_64_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
dist4_64_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo

dist7_75_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
dist5_75_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo

global_count = 0

mtx = np.array([[670.84500123, 0., 958.33741765], [0., 675.08780047, 548.98233789], [0., 0., 1.]])
mtxK4 = np.array([[1.06273552e+03, 0., 9.58672062e+02], [0., 1.18756297e+03, 5.44264218e+02], [0., 0., 1.]])

R = np.array([[0.99146, 0.063475, -0.1139],
              [-0.067961, 0.99704, -0.035938],
              [0.11128, 0.043372, 0.99284]])
t = np.array([[-0.96251], [0.057667], [-0.26504]])


def convert3d():
    points1__ = np.append(distorigin4, filterPoints1, axis=0)
    points2__ = np.append(distorigin5, filterPoints2, axis=0)

    # Projektion der Punkte in homogene Koordinaten
    points1_hom = cv2.convertPointsToHomogeneous(points1__).reshape(-1, 3).T
    points2_hom = cv2.convertPointsToHomogeneous(points2__).reshape(-1, 3).T

    # Triangulation der 3D-Punkte
    P1 = np.dot(mtx, np.hstack((np.eye(3), np.zeros((3, 1)))))  # Kamera 1 Projektion: K1[I | 0]
    P2 = np.dot(mtx, np.hstack((R, t)))  # Kamera 2 Projektion: K2[R | t]

    points4D = cv2.triangulatePoints(P1, P2, points1_hom[:2], points2_hom[:2])
    points3D = points4D / points4D[3]

    print("3D-Punkte mit Kalibrierung:")
    points3D = points3D[:3].T

    for row in points3D:
        string = "["
        for element in row:
            string = string + str(element) + ','
        string = string[:-1] + '],'
        print(string)

    #pca = PCA(n_components=3)
    #points3D = pca.fit_transform(points3D)

    def rotate_y(points, angle_degrees):
        angle_radians = np.radians(angle_degrees)
        cos_theta = np.cos(angle_radians)
        sin_theta = np.sin(angle_radians)
        rotation_matrix = np.array([
            [cos_theta, 0, sin_theta],
            [0, 1, 0],
            [-sin_theta, 0, cos_theta]
        ])
        return np.dot(points, rotation_matrix)

    # 3D-Punkte um 90 Grad um die Y-Achse drehen
    #points3D = rotate_y(points3D, 270)

    P0 = points3D[0]
    P1 = points3D[1]
    P2 = points3D[2]
    # Translation
    Torigin = -P0

    # Neue X-Achse
    new_x_axis = (P1 - P0) / np.linalg.norm(P1 - P0)

    # Neue Z-Achse
    new_z_axis = np.cross(P1 - P0, P2 - P0)
    new_z_axis /= np.linalg.norm(new_z_axis)

    # Neue Y-Achse
    new_y_axis = np.cross(new_z_axis, new_x_axis)

    # Rotationsmatrix
    Rorigin = np.vstack([new_x_axis, new_y_axis, new_z_axis]).T

    temp = Rorigin @ (points3D.T + Torigin[:, np.newaxis])
    points3Dorigin = temp.T


while True:
    start_time = time.time()

    # Capture frame-by-frame
    ret1, frame1 = cap1.read()
    ret2, frame2 = cap2.read()
    if not ret1 or not ret2:
        break

    # Run YOLO model on the frame
    results1 = model(frame1)
    results2 = model(frame2)

    cv2.imwrite(f'detect1.png', results1[0].plot())
    cv2.imwrite(f'detect2.png', results2[0].plot())

    if len(results1[0].boxes) == 1 and len(results2[0].boxes) == 1:
        # gpu
        resultsgpu1 = results1[0].cpu()
        resultsgpu2 = results2[0].cpu()

        # Assuming the results[0] contains the annotated frame
        # gpu
        annotated_frame1 = resultsgpu1.plot()  # plot the results on the frame
        annotated_frame2 = resultsgpu2.plot()  # plot the results on the frame

        # if cpu
        #annotated_frame1 = results1[0].plot()  # plot the results on the frame
        #annotated_frame2 = results2[0].plot()  # plot the results on the frame

        # gpu
        keypoints1 = resultsgpu1.keypoints.xy
        keypoints2 = resultsgpu2.keypoints.xy

        # cpu
        #keypoints1 = results1[0].keypoints.xy
        #keypoints2 = results2[0].keypoints.xy

        if keypoints1 is not None and keypoints1.shape[0] > 0:

            # In numpy arrays umwandeln
            allPoints1 = np.array(keypoints1[0], dtype=np.float32)
            allPoints2 = np.array(keypoints2[0], dtype=np.float32)

            valid_indices = [(x != [0, 0]).all() and (y != [0, 0]).all() for x, y in zip(allPoints1, allPoints2)]

            # Filtere die Listen
            filterPoints1 = allPoints1[valid_indices]
            filterPoints2 = allPoints2[valid_indices]

            convert3d()
            global_count = global_count + 1

        else:
            raise ValueError("Ein oder beide Keypoints-Arrays sind leer.")

        # Display the frame

        # Calculate the time taken to process the frame
        end_time = time.time()
        elapsed_time = end_time - start_time

        # Sleep to maintain the desired FPS
        time_to_sleep = frame_time - elapsed_time
        if time_to_sleep > 0:
            time.sleep(time_to_sleep)

        # Break the loop if 'q' is pressed
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

# Release the video capture object and close all OpenCV windows
cap1.release()
cap2.release()
cv2.destroyAllWindows()
