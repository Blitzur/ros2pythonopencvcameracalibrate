# ros stuff
import cv2
import rclpy
from geometry_msgs.msg import Point
from matplotlib import pyplot as plt
from rclpy.node import Node
from std_msgs.msg import Header
from visualization_msgs.msg import Marker
from sensor_msgs.msg import PointCloud2, PointField
import sensor_msgs_py.point_cloud2 as pc2

# open CV stuff
import cv2
import numpy as np
import time
import torch
from ultralytics import YOLO

source1 = "human4.mp4"
source2 = "human5.mp4"
source3 = "human6.mp4"

# Load the YOLO model
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = YOLO('yolov8n-pose.pt')

# Set desired FPS
desired_fps = 25
frame_time = 1.0 / desired_fps

# Kalibrierungsdaten von OpenCV (calcRt.py)
R12 = np.array([[-0.16299702, 0.34712577, -0.92354517], [-0.25376075, 0.88982281, 0.37923719],
                [0.95343456, 0.29617405, -0.05695158]], dtype=np.float32)
R23 = np.array([[0.10202337, 0.00616194, -0.99476292], [-0.51202085, 0.85767522, -0.04720034],
                [0.85289265, 0.51415489, 0.09065796]], dtype=np.float32)
R31 = np.array([[-0.96931956, -0.19004095, -0.1558975], [-0.22855112, 0.46339564, 0.85617105],
                [-0.09046534, 0.86553389, -0.49261253]], dtype=np.float32)
t12 = np.array([[0.69496035], [-0.09293747], [0.71301665]], dtype=np.float32)
t23 = np.array([[0.7805161], [-0.14153878], [0.60890179]], dtype=np.float32)
t31 = np.array([[0.35251358], [-0.37031337], [0.85941968]], dtype=np.float32)

Kmtx = np.array(
    [[670.84500123, 0., 958.33741765], [0., 675.08780047, 548.98233789], [0., 0., 1.]])  # currently one matrix for all

#K6 = np.array([[1.47141658e+03 0.00000000e+00 9.64257828e+02], [0.00000000e+00 5.46470684e+03 5.41542642e+02], [0.00000000e+00 0.00000000e+00 1.00000000e+00]])
#dist6 = np.array([[-0.38964264 -0.10744518 -0.07632136  0.11121348  6.51572193]])

# 3 origin ref points that ALL sources need to be able to see
distorigin1 = np.array([[1088, 795], [1059, 776], [1125, 772]])  # box bottom right, bottom left, up right
distorigin2 = np.array([[1046, 830], [1023, 851], [1007, 816]])  # box bottom right, bottom left, up right
distorigin3 = np.array([[1103, 559], [1127, 576], [1072, 575]])  # box bottom right, bottom left, up right


def rotate_z_to_x_axis(points):
    # P0 und P1 als numpy arrays vorbereiten
    p0 = points[0]
    p1 = points[1]

    # Richtungsvektor der Geraden durch P0 und P1
    v = p1 - p0

    # Einheitsvektor in Richtung der X-Achse
    ex = np.array([1, 0, 0])

    # Winkel zwischen v und ex berechnen
    cos_theta = np.dot(v, ex) / (np.linalg.norm(v) * np.linalg.norm(ex))
    theta = np.arccos(cos_theta)

    # Drehachse bestimmen (Normalenvektor zu v und ex)
    n = np.cross(v, ex)
    n /= np.linalg.norm(n)  # Einheitsvektor machen

    # Rotationsmatrix um die Achse n um den Winkel theta
    Rot = np.array([[np.cos(theta) + n[0] ** 2 * (1 - np.cos(theta)),
                     n[0] * n[1] * (1 - np.cos(theta)) - n[2] * np.sin(theta),
                     n[0] * n[2] * (1 - np.cos(theta)) + n[1] * np.sin(theta)],
                    [n[1] * n[0] * (1 - np.cos(theta)) + n[2] * np.sin(theta),
                     np.cos(theta) + n[1] ** 2 * (1 - np.cos(theta)),
                     n[1] * n[2] * (1 - np.cos(theta)) - n[0] * np.sin(theta)],
                    [n[2] * n[0] * (1 - np.cos(theta)) - n[1] * np.sin(theta),
                     n[2] * n[1] * (1 - np.cos(theta)) + n[0] * np.sin(theta),
                     np.cos(theta) + n[2] ** 2 * (1 - np.cos(theta))]])

    # Punkte auf der Ebene rotieren (falls benötigt)
    # Hier wird angenommen, dass die Punkte auf der Ebene durch P0 und P1 gegeben sind.
    # Wenn die Punkte auf einer anderen Ebene liegen, sollte der Rotationsvorgang entsprechend angepasst werden.

    pointsRotated = np.dot(points, Rot.T)

    return pointsRotated


def translate_point_cloud(points):
    translation_vector = -points[0]
    # Translate all points
    translated_points = points + translation_vector
    return translated_points


def translate_to_origin(points):
    # Translate points so that point 1 is at the origin
    p1 = points[0]
    translated_points = points - p1
    return translated_points


def rotate_around_z(points, angle):
    # Rotate points around the Z axis by the given angle
    cos_angle = np.cos(angle)
    sin_angle = np.sin(angle)
    rotation_matrix = np.array([
        [cos_angle, -sin_angle, 0],
        [sin_angle, cos_angle, 0],
        [0, 0, 1]
    ])
    return points @ rotation_matrix.T


def rotate_around_y(points, angle):
    # Rotate points around the Y axis by the given angle
    cos_angle = np.cos(angle)
    sin_angle = np.sin(angle)
    rotation_matrix = np.array([
        [cos_angle, 0, sin_angle],
        [0, 1, 0],
        [-sin_angle, 0, cos_angle]
    ])
    return points @ rotation_matrix.T


def rotate_x(points, angle_degrees):
    angle_rad = np.radians(angle_degrees)
    rotation_matrix = np.array([
        [1, 0, 0],
        [0, np.cos(angle_rad), -np.sin(angle_rad)],
        [0, np.sin(angle_rad), np.cos(angle_rad)]
    ])
    rotated_points = np.dot(points, rotation_matrix.T)
    return rotated_points


class CameraPositionPublisher(Node):
    def __init__(self):
        super().__init__('camera_position_publisher')

        # Open the video source (0 for webcam, or provide a video file path)
        self.cap1 = cv2.VideoCapture(source1)
        self.cap2 = cv2.VideoCapture(source2)
        self.cap3 = cv2.VideoCapture(source3)

        # Check if video source opened successfully
        if not self.cap1.isOpened() or not self.cap2.isOpened() or not self.cap3.isOpened():
            print("Error: Could not open video source.")
            exit()

        self.points_pub1 = self.create_publisher(PointCloud2, 'points1', 1)
        self.points_pub2 = self.create_publisher(PointCloud2, 'points2', 1)
        self.points_pub3 = self.create_publisher(PointCloud2, 'points3', 1)
        self.markers_pub1 = self.create_publisher(Marker, 'lines1', 1)
        self.markers_pub2 = self.create_publisher(Marker, 'lines2', 1)
        self.markers_pub3 = self.create_publisher(Marker, 'lines3', 1)
        self.global_count = 0

        self.mainloop()

    def mainloop(self):
        while True:
            start_time = time.time()

            ret1, frame1 = self.cap1.read()
            ret2, frame2 = self.cap2.read()
            ret3, frame3 = self.cap3.read()

            if not ret1 or not ret2 or not ret3:
                break

            # Run YOLO model on the dist frame
            gpuresult1 = model(frame1)
            gpuresult2 = model(frame2)
            gpuresult3 = model(frame3)

            # todo make a single method that will do all the 3D point stuff with 2 2D points etc
            # todo get the 3D points for current frame and merge into one :D
            if len(gpuresult1[0].boxes) == 1 and len(gpuresult2[0].boxes) == 1 and len(gpuresult3[0].boxes) == 1:  # we need at least two skeleton detections in order to continue
                cpuResult1 = gpuresult1[0].cpu()
                cpuResult2 = gpuresult2[0].cpu()
                cpuResult3 = gpuresult3[0].cpu()

                # Assuming the results[0] contains the annotated frame
                yoloFrame1 = cpuResult1.plot()
                yoloFrame2 = cpuResult2.plot()
                yoloFrame3 = cpuResult3.plot()

                keypoints1 = cpuResult1.keypoints.xy
                keypoints2 = cpuResult2.keypoints.xy
                keypoints3 = cpuResult3.keypoints.xy

                if keypoints1 is not None and keypoints1.shape[0] > 0:

                    # In numpy arrays umwandeln
                    allPoints1 = np.array(keypoints1[0], dtype=np.float32)
                    allPoints2 = np.array(keypoints2[0], dtype=np.float32)
                    allPoints3 = np.array(keypoints3[0], dtype=np.float32)

                    valid_indices12 = [(x != [0, 0]).all() and (y != [0, 0]).all() for x, y in
                                       zip(allPoints1, allPoints2)]
                    valid_indices23 = [(x != [0, 0]).all() and (y != [0, 0]).all() for x, y in
                                       zip(allPoints2, allPoints3)]
                    valid_indices31 = [(x != [0, 0]).all() and (y != [0, 0]).all() for x, y in
                                       zip(allPoints3, allPoints1)]

                    # Filtere die Listen
                    filterPoints12_1 = allPoints1[valid_indices12]
                    filterPoints12_2 = allPoints2[valid_indices12]

                    filterPoints23_1 = allPoints2[valid_indices23]
                    filterPoints23_2 = allPoints3[valid_indices23]

                    filterPoints31_1 = allPoints3[valid_indices31]
                    filterPoints31_2 = allPoints1[valid_indices31]

                    # add origin points
                    filterPoints12_1 = np.append(distorigin1, filterPoints12_1, axis=0)
                    filterPoints12_2 = np.append(distorigin2, filterPoints12_2, axis=0)

                    filterPoints23_1 = np.append(distorigin2, filterPoints23_1, axis=0)
                    filterPoints23_2 = np.append(distorigin3, filterPoints23_2, axis=0)

                    filterPoints31_1 = np.append(distorigin3, filterPoints31_1, axis=0)
                    filterPoints31_2 = np.append(distorigin1, filterPoints31_2, axis=0)

                    transformed3D1 = self.convert3d(filterPoints12_1, filterPoints12_2, valid_indices12, R12, t12)
                    transformed3D2 = self.convert3d(filterPoints23_1, filterPoints23_2, valid_indices23, R23, t23)
                    transformed3D3 = self.convert3d(filterPoints31_1, filterPoints31_2, valid_indices31, R31, t31)

                    # scale points by P0 -> P1
                    factor_x12 = transformed3D1[1][0] / transformed3D2[1][0]

                    for point in transformed3D2:
                        point[0] *= factor_x12

                    factor_x13 = transformed3D1[1][0] / transformed3D3[1][0]

                    for point in transformed3D3:
                        point[0] *= factor_x13

                    if len(valid_indices12) == 0:
                        print('error')

                    pc_1 = self.create_point_cloud(transformed3D1)
                    self.points_pub1.publish(pc_1)
                    pc_2 = self.create_point_cloud(transformed3D2)
                    self.points_pub2.publish(pc_2)
                    pc_3 = self.create_point_cloud(transformed3D3)
                    self.points_pub3.publish(pc_3)
                    lm1 = self.create_line_marker(transformed3D1, valid_indices12)
                    self.markers_pub1.publish(lm1)
                    lm2 = self.create_line_marker(transformed3D2, valid_indices23)
                    self.markers_pub2.publish(lm2)
                    lm3 = self.create_line_marker(transformed3D3, valid_indices31)
                    self.markers_pub3.publish(lm3)

                    self.global_count = self.global_count + 1
                else:
                    raise ValueError("Ein oder beide Keypoints-Arrays sind leer.")

                # Display the frame

                # Calculate the time taken to process the frame
                end_time = time.time()
                elapsed_time = end_time - start_time

                # Sleep to maintain the desired FPS
                time_to_sleep = frame_time - elapsed_time
                if time_to_sleep > 0:
                    time.sleep(time_to_sleep)

        # Release the video capture object and close all OpenCV windows
        self.cap1.release()
        self.cap2.release()

    def convert3d(self, filterPoints1, filterPoints2, valid_indices, R_, t_):
        # Projektion der Punkte in homogene Koordinaten
        points1_hom = cv2.convertPointsToHomogeneous(filterPoints1).reshape(-1, 3).T
        points2_hom = cv2.convertPointsToHomogeneous(filterPoints2).reshape(-1, 3).T

        # Triangulation der 3D-Punkte
        P1 = np.dot(Kmtx, np.hstack((np.eye(3), np.zeros((3, 1)))))  # Kamera 1 Projektion: K1[I | 0]
        P2 = np.dot(Kmtx, np.hstack((R_, t_)))  # Kamera 2 Projektion: K2[R | t]

        points4D = cv2.triangulatePoints(P1, P2, points1_hom[:2], points2_hom[:2])
        points3D = points4D / points4D[3]

        #print("3D-Punkte mit Kalibrierung:")
        points3D = points3D[:3].T

        def calculate_normal_vector(_p1, _p2, _p3):
            # Berechne die Vektoren u und v
            u = np.array(_p2) - np.array(_p1)
            v = np.array(_p3) - np.array(_p1)

            # Berechne den Normalenvektor
            n = np.cross(u, v)
            n_normalized = n / np.linalg.norm(n)

            return n_normalized

        def calculate_rotation_matrix(_normal):
            # Der Zielvektor ist (0, 0, 1)
            z_axis = np.array([0, 0, 1])

            # Berechne die Drehachse
            k = np.cross(_normal, z_axis)
            k_normalized = k / np.linalg.norm(k)

            # Berechne den Drehwinkel
            cos_theta = np.dot(_normal, z_axis)
            theta = np.arccos(cos_theta)

            # Erzeuge die Kreuzproduktmatrix K
            kx, ky, kz = k_normalized
            K = np.array([[0, -kz, ky],
                          [kz, 0, -kx],
                          [-ky, kx, 0]])

            # Berechne die Rotationsmatrix
            _I = np.identity(3)
            _Rot = _I + np.sin(theta) * K + (1 - np.cos(theta)) * np.dot(K, K)

            return _Rot

        # refpunkte
        p1 = points3D[0]
        p2 = points3D[1]
        p3 = points3D[2]

        points3D = np.append(points3D, np.array([[0.0, 0.0, 0.0], [0.0, 0.0, -0.1]]), axis=0)

        # Berechne den Normalenvektor der Ebene
        normal = calculate_normal_vector(p1, p2, p3)

        # Berechne die Rotationsmatrix
        Rot = calculate_rotation_matrix(normal)

        transformed_points = np.dot(points3D, Rot.T)

        transformed_points = rotate_x(transformed_points, 180.0)

        transformed_points = translate_point_cloud(transformed_points)

        transformed_points = rotate_z_to_x_axis(transformed_points)

        ##########################################################################

        for e in transformed_points:
            e[0] *= 5.0
            e[1] *= 5.0
            e[2] *= 5.0

        #pc = self.create_point_cloud(transformed_points, valid_indices)
        #self.points_pub.publish(pc)
        #lm = self.create_line_marker(transformed_points, valid_indices)
        #self.markers_pub.publish(lm)
        return transformed_points

    def create_line_marker(self, points, valid_indices):
        marker = Marker()
        marker.header.frame_id = "base_link"
        marker.header.stamp = self.get_clock().now().to_msg()
        marker.ns = "lines"
        marker.id = 0
        marker.type = Marker.LINE_LIST
        marker.action = Marker.ADD
        marker.scale.x = 0.05
        marker.color.a = 1.0
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0

        # ref board floor
        marker.points.append(Point(x=float(points[0][0]), y=float(points[0][1]), z=float(points[0][2])))
        marker.points.append(Point(x=float(points[1][0]), y=float(points[1][1]), z=float(points[1][2])))
        marker.points.append(Point(x=float(points[2][0]), y=float(points[2][1]), z=float(points[2][2])))
        marker.points.append(Point(x=float(points[1][0]), y=float(points[1][1]), z=float(points[1][2])))
        marker.points.append(Point(x=float(points[2][0]), y=float(points[2][1]), z=float(points[2][2])))
        marker.points.append(Point(x=float(points[0][0]), y=float(points[0][1]), z=float(points[0][2])))

        # kamera position an direction
        marker.points.append(Point(x=float(points[len(points) - 2][0]), y=float(points[len(points) - 2][1]),
                                   z=float(points[len(points) - 2][2])))
        marker.points.append(Point(x=float(points[len(points) - 1][0]), y=float(points[len(points) - 1][1]),
                                   z=float(points[len(points) - 1][2])))

        pointsWithGaps = []
        temp = 3
        for b in valid_indices:
            if b:
                pointsWithGaps.append([points[temp][0], points[temp][1], points[temp][2]])
                temp += 1
            else:
                pointsWithGaps.append([0.0, 0.0, 0.0])

        #print(pointsWithGaps)

        if valid_indices[0]:  # nase
            if valid_indices[1]:  # auf auge1
                marker.points.append(
                    Point(x=float(pointsWithGaps[0][0]), y=float(pointsWithGaps[0][1]), z=float(pointsWithGaps[0][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[1][0]), y=float(pointsWithGaps[1][1]), z=float(pointsWithGaps[1][2])))
            if valid_indices[2]:  # auf auge2
                marker.points.append(
                    Point(x=float(pointsWithGaps[0][0]), y=float(pointsWithGaps[0][1]), z=float(pointsWithGaps[0][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[2][0]), y=float(pointsWithGaps[2][1]), z=float(pointsWithGaps[2][2])))
        if valid_indices[3]:  # ohr1
            if valid_indices[5]:  # auf schulter1
                marker.points.append(
                    Point(x=float(pointsWithGaps[3][0]), y=float(pointsWithGaps[3][1]), z=float(pointsWithGaps[3][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[5][0]), y=float(pointsWithGaps[5][1]), z=float(pointsWithGaps[5][2])))
        if valid_indices[4]:  # ohr2
            if valid_indices[6]:  # auf schulter2
                marker.points.append(
                    Point(x=float(pointsWithGaps[4][0]), y=float(pointsWithGaps[4][1]), z=float(pointsWithGaps[4][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[6][0]), y=float(pointsWithGaps[6][1]), z=float(pointsWithGaps[6][2])))
        if valid_indices[5]:  # ohr2
            if valid_indices[6]:  # auf schulter2
                marker.points.append(
                    Point(x=float(pointsWithGaps[5][0]), y=float(pointsWithGaps[5][1]), z=float(pointsWithGaps[5][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[6][0]), y=float(pointsWithGaps[6][1]), z=float(pointsWithGaps[6][2])))
        if valid_indices[8]:  # ohr2
            if valid_indices[6]:  # auf schulter2
                marker.points.append(
                    Point(x=float(pointsWithGaps[8][0]), y=float(pointsWithGaps[8][1]), z=float(pointsWithGaps[8][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[6][0]), y=float(pointsWithGaps[6][1]), z=float(pointsWithGaps[6][2])))
        if valid_indices[5]:  # ohr2
            if valid_indices[7]:  # auf schulter2
                marker.points.append(
                    Point(x=float(pointsWithGaps[5][0]), y=float(pointsWithGaps[5][1]), z=float(pointsWithGaps[5][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[7][0]), y=float(pointsWithGaps[7][1]), z=float(pointsWithGaps[7][2])))
        if valid_indices[9]:  # ohr2
            if valid_indices[7]:  # auf schulter2
                marker.points.append(
                    Point(x=float(pointsWithGaps[9][0]), y=float(pointsWithGaps[9][1]), z=float(pointsWithGaps[9][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[7][0]), y=float(pointsWithGaps[7][1]), z=float(pointsWithGaps[7][2])))
        if valid_indices[8]:  # ohr2
            if valid_indices[10]:  # auf schulter2
                marker.points.append(
                    Point(x=float(pointsWithGaps[8][0]), y=float(pointsWithGaps[8][1]), z=float(pointsWithGaps[8][2])))
                marker.points.append(Point(x=float(pointsWithGaps[10][0]), y=float(pointsWithGaps[10][1]),
                                           z=float(pointsWithGaps[10][2])))
        if valid_indices[5]:  # schulter hüfte
            if valid_indices[11]:
                marker.points.append(
                    Point(x=float(pointsWithGaps[5][0]), y=float(pointsWithGaps[5][1]), z=float(pointsWithGaps[5][2])))
                marker.points.append(Point(x=float(pointsWithGaps[11][0]), y=float(pointsWithGaps[11][1]),
                                           z=float(pointsWithGaps[11][2])))
        if valid_indices[6]:  # schulter hüfte
            if valid_indices[12]:
                marker.points.append(
                    Point(x=float(pointsWithGaps[6][0]), y=float(pointsWithGaps[6][1]), z=float(pointsWithGaps[6][2])))
                marker.points.append(Point(x=float(pointsWithGaps[12][0]), y=float(pointsWithGaps[12][1]),
                                           z=float(pointsWithGaps[12][2])))
        if valid_indices[11]:  # schulter hüfte
            if valid_indices[12]:
                marker.points.append(Point(x=float(pointsWithGaps[11][0]), y=float(pointsWithGaps[11][1]),
                                           z=float(pointsWithGaps[11][2])))
                marker.points.append(Point(x=float(pointsWithGaps[12][0]), y=float(pointsWithGaps[12][1]),
                                           z=float(pointsWithGaps[12][2])))
        if valid_indices[11]:  # schulter hüfte
            if valid_indices[13]:
                marker.points.append(Point(x=float(pointsWithGaps[11][0]), y=float(pointsWithGaps[11][1]),
                                           z=float(pointsWithGaps[11][2])))
                marker.points.append(Point(x=float(pointsWithGaps[13][0]), y=float(pointsWithGaps[13][1]),
                                           z=float(pointsWithGaps[13][2])))
        if valid_indices[14]:  # schulter hüfte
            if valid_indices[12]:
                marker.points.append(Point(x=float(pointsWithGaps[14][0]), y=float(pointsWithGaps[14][1]),
                                           z=float(pointsWithGaps[14][2])))
                marker.points.append(Point(x=float(pointsWithGaps[12][0]), y=float(pointsWithGaps[12][1]),
                                           z=float(pointsWithGaps[12][2])))
        if valid_indices[15]:  # schulter hüfte
            if valid_indices[13]:
                marker.points.append(Point(x=float(pointsWithGaps[15][0]), y=float(pointsWithGaps[15][1]),
                                           z=float(pointsWithGaps[15][2])))
                marker.points.append(Point(x=float(pointsWithGaps[13][0]), y=float(pointsWithGaps[13][1]),
                                           z=float(pointsWithGaps[13][2])))
        if valid_indices[14]:  # schulter hüfte
            if valid_indices[16]:
                marker.points.append(Point(x=float(pointsWithGaps[14][0]), y=float(pointsWithGaps[14][1]),
                                           z=float(pointsWithGaps[14][2])))
                marker.points.append(Point(x=float(pointsWithGaps[16][0]), y=float(pointsWithGaps[16][1]),
                                           z=float(pointsWithGaps[16][2])))

        return marker

    def create_point_cloud(self, points):
        header = Header()
        header.stamp = self.get_clock().now().to_msg()
        header.frame_id = 'base_link'

        fields = [
            PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1),
        ]

        pc = pc2.create_cloud(header, fields, points)
        return pc


def main(args=None):
    rclpy.init(args=args)
    camera_position_publisher = CameraPositionPublisher()
    rclpy.spin(camera_position_publisher)
    camera_position_publisher.destroy_node()
    rclpy.shutdown()


if True:
    main()
