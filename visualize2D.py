import numpy as np
import matplotlib.pyplot as plt

# Beispiel-2D-Daten
points1 = np.array([
    [394.29, 141.79],
    [401.07, 136.78],
    [388.96, 136.00],
    [411.78, 141.56],
    [382.67, 140.38],
    [425.26, 180.14],
    [371.29, 180.66],
    [439.48, 225.00],
    [350.65, 226.39],
    [454.30, 263.79],
    [327.40, 257.36],
    [418.33, 276.59],
    [383.17, 275.69],
    [424.44, 346.64],
    [377.12, 338.28],
    [430.81, 413.21],
    [379.80, 401.59]
])

# Erstelle den 2D-Plot
plt.figure()
plt.scatter(points1[:, 0], points1[:, 1])
plt.title('2D Plot der Punkte')
plt.xlabel('X-Achse')
plt.ylabel('Y-Achse')

# Speichere den Plot als Bilddatei (z.B. PNG)
plt.savefig('2d_plot.png')

# Zeige den Plot (optional)
plt.show()
