import numpy as np
import rclpy
from rclpy.node import Node
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point


def calculate_corners(width, height, focal_length):
    corners = [Point(x=width / 2, y=height / 2, z=float(focal_length)),
               Point(x=-width / 2, y=height / 2, z=float(focal_length)),
               Point(x=-width / 2, y=-height / 2, z=float(focal_length)),
               Point(x=width / 2, y=-height / 2, z=float(focal_length))]
    return corners


class CameraVisualization(Node):
    def __init__(self):
        super().__init__('camera_visualization')
        self.publisher1_ = self.create_publisher(Marker, 'camera_marker1', 1)
        self.publisher2_ = self.create_publisher(Marker, 'camera_marker2', 1)
        self.timer1 = self.create_timer(0.5, self.publish_markers1)
        self.timer2 = self.create_timer(0.5, self.publish_markers2)

        self.fx = 670.84500123
        self.cx = 958.33741765
        self.fy = 675.08780047
        self.cy = 548.98233789
        self.camera_matrix = np.array([[self.fx, 0, self.cx],
                                       [0, self.fy, self.cy],
                                       [0, 0, 1]])
        self.R = np.array([[-0.16299702, 0.34712577, -0.92354517], [-0.25376075, 0.88982281, 0.37923719],
                      [0.95343456, 0.29617405, -0.05695158]], dtype=np.float32)
        self.t = np.array([[0.69496035], [-0.09293747], [0.71301665]], dtype=np.float32)

        pixel_per_meter = 800

        self.declare_parameter('image_width', 2 * self.cx / pixel_per_meter)
        self.declare_parameter('image_height', 2 * self.cy / pixel_per_meter)
        self.declare_parameter('focal_length', (self.fx + self.fy) / 2 / pixel_per_meter)

    def publish_markers1(self):
        image_width = self.get_parameter('image_width').get_parameter_value().double_value
        image_height = self.get_parameter('image_height').get_parameter_value().double_value
        focal_length = self.get_parameter('focal_length').get_parameter_value().double_value

        marker = Marker()
        marker.header.frame_id = "base_link"
        marker.header.stamp = self.get_clock().now().to_msg()
        marker.ns = "camera"
        marker.id = 0
        marker.type = Marker.LINE_STRIP
        marker.action = Marker.ADD
        marker.scale.x = 0.01
        marker.color.a = 1.0
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0

        corners = calculate_corners(image_width, image_height, focal_length)

        marker.points.append(Point(x=0.0, y=0.0, z=0.0))
        for corner in corners:
            marker.points.append(corner)
            marker.points.append(Point(x=0.0, y=0.0, z=0.0))
        for i in range(1, len(corners)):
            marker.points.append(corners[i - 1])
            marker.points.append(corners[i])
        marker.points.append(corners[0])
        marker.points.append(corners[3])

        self.publisher1_.publish(marker)

    def publish_markers2(self):
        image_width = self.get_parameter('image_width').get_parameter_value().double_value
        image_height = self.get_parameter('image_height').get_parameter_value().double_value
        focal_length = self.get_parameter('focal_length').get_parameter_value().double_value

        marker = Marker()
        marker.header.frame_id = "base_link"
        marker.header.stamp = self.get_clock().now().to_msg()
        marker.ns = "camera"
        marker.id = 0
        marker.type = Marker.LINE_STRIP
        marker.action = Marker.ADD
        marker.scale.x = 0.01
        marker.color.a = 1.0
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0

        corners = calculate_corners(image_width, image_height, focal_length)

        # Kombiniere R und t zu einer Transformationsmatrix T
        T = np.eye(4)
        T[:3, :3] = self.R
        T[:3, 3] = self.t.flatten()

        # Punkt in Kamera 1 Koordinaten
        point_camera1 = np.array([[0.0],
                                  [0.0],
                                  [0.0],
                                  [1]])  # Homogene Koordinaten

        # Transformiere den Punkt in Kamera 2 Koordinaten
        point_camera2 = np.dot(T, point_camera1)

        # Extrahiere die transformierten Koordinaten aus homogenen Koordinaten
        transformed_x = point_camera2[0, 0]
        transformed_y = point_camera2[1, 0]
        transformed_z = point_camera2[2, 0]

        corners2 = []
        marker.points.append(Point(x=transformed_x, y=transformed_y, z=transformed_z))
        for corner in corners:
            temp = np.array([[corner.x], [corner.y], [corner.z], [1]])
            temp = np.dot(T, temp)
            point = Point(x=temp[0, 0], y=temp[1, 0], z=temp[2, 0])
            marker.points.append(point)
            corners2.append(point)
            marker.points.append(Point(x=transformed_x, y=transformed_y, z=transformed_z))
        for i in range(1, len(corners2)):
            marker.points.append(corners2[i - 1])
            marker.points.append(corners2[i])
        marker.points.append(corners2[0])
        marker.points.append(corners2[3])

        self.publisher2_.publish(marker)


def main(args=None):
    rclpy.init(args=args)
    node = CameraVisualization()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
