import cv2
import rclpy
from rclpy.node import Node
from std_msgs.msg import Header
from visualization_msgs.msg import Marker
from sensor_msgs.msg import PointCloud2, PointField
import sensor_msgs_py.point_cloud2 as pc2
import numpy as np


class CameraPositionPublisher(Node):
    def __init__(self):
        super().__init__('camera_position_publisher')

        self.points_pub = self.create_publisher(PointCloud2, 'points', 10)
        self.markers_pub = self.create_publisher(Marker, 'camera_markers', 10)

        self.timer = self.create_timer(1.0, self.publish_data)  # 1 Hz

        #Kalibrierungsdaten von OpenCV
        R = np.array([[0.99146, 0.063475, -0.1139],
                      [-0.067961, 0.99704, -0.035938],
                      [0.11128, 0.043372, 0.99284]], dtype=np.float32)
        t = np.array([[-0.96251], [0.057667], [-0.26504]], dtype=np.float32)

        mtx = np.array([
            [670.84500123, 0., 958.33741765],
            [0., 675.08780047, 548.98233789],
            [0., 0., 1.]
        ])

        # 2D Referenzpunkte         origin         x            y
        distorigin4 = np.array([[1088, 795], [1057, 776], [1006, 816]], dtype=np.float32)
        distorigin5 = np.array([[1047, 830], [1023, 851], [1006, 816]], dtype=np.float32)

        points1_hom = cv2.convertPointsToHomogeneous(distorigin4).reshape(-1, 3).T
        points2_hom = cv2.convertPointsToHomogeneous(distorigin5).reshape(-1, 3).T

        # Triangulation der 3D-Punkte
        P1 = np.dot(mtx, np.hstack((np.eye(3), np.zeros((3, 1)))))  # Kamera 1 Projektion: K1[I | 0]
        P2 = np.dot(mtx, np.hstack((R, t)))  # Kamera 2 Projektion: K2[R | t]

        points4D = cv2.triangulatePoints(P1, P2, points1_hom[:2], points2_hom[:2])
        points3D = points4D / points4D[3]

        print("3D-Punkte mit Kalibrierung:")
        points3D = points3D[:3].T

        # 3D refezenzpunkte
        p1 = points3D[0]
        p2 = points3D[1]
        p3 = points3D[2]

        # Ursprung und Transformation definieren
        origin = p1
        v1 = p2 - p1
        v2 = p3 - p1
        normal = np.cross(v1, v2)
        v1_norm = v1 / np.linalg.norm(v1)
        v2_norm = v2 / np.linalg.norm(v2)
        normal_norm = normal / np.linalg.norm(normal)

        R_world = np.vstack((v1_norm, v2_norm, normal_norm)).T
        t_world = -origin

        # Kamera 1 Position im globalen Koordinatensystem (angenommen p1 als Ursprungsposition)
        self.camera1_position = np.dot(R_world, origin) + t_world

        # Kamera 2 Position im globalen Koordinatensystem
        camera2_position_local = -np.dot(R.T, t)
        self.camera2_position = np.dot(R_world, camera2_position_local.reshape(3, 1)).flatten() + t_world

        # Punkte definieren
        self.points = np.array([points3D])

    def publish_data(self):
        pc = self.create_point_cloud(self.points)
        self.points_pub.publish(pc)

        marker1 = self.create_camera_marker(self.camera1_position, [0, 0, 0, 1], 1)
        marker2 = self.create_camera_marker(self.camera2_position, [0, 0, 0, 1], 2)

        self.markers_pub.publish(marker1)
        self.markers_pub.publish(marker2)

    def create_camera_marker(self, position, orientation, id_):
        marker = Marker()
        marker.header.frame_id = "map"
        marker.header.stamp = self.get_clock().now().to_msg()
        marker.ns = "cameras"
        marker.id = id_
        marker.type = Marker.ARROW
        marker.action = Marker.ADD
        print(float(position[0]))
        print(position[0])
        print(float(orientation[0]))
        print(orientation[0])

        marker.pose.position.x = float(position[0])
        marker.pose.position.y = float(position[1])
        marker.pose.position.z = float(position[2])

        marker.pose.orientation.x = float(orientation[0])
        marker.pose.orientation.y = float(orientation[1])
        marker.pose.orientation.z = float(orientation[2])
        marker.pose.orientation.w = float(orientation[3])

        marker.scale.x = 0.5  # Length of the arrow
        marker.scale.y = 0.1  # Width of the arrow
        marker.scale.z = 0.1  # Height of the arrow

        marker.color.a = 1.0
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0

        return marker

    def create_point_cloud(self, points):
        header = Header()
        header.stamp = self.get_clock().now().to_msg()
        header.frame_id = 'map'

        fields = [
            PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1),
        ]

        pc = pc2.create_cloud(header, fields, points)
        return pc

def main(args=None):
    rclpy.init(args=args)
    camera_position_publisher = CameraPositionPublisher()
    rclpy.spin(camera_position_publisher)
    camera_position_publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
