import cv2
import time
from ultralytics import YOLO

# Load the YOLO model
model = YOLO('yolov8n-pose.pt')

# Open the video source (0 for webcam, or provide a video file path)
video_source = 2
cap = cv2.VideoCapture(video_source)

# Check if video source opened successfully
if not cap.isOpened():
    print("Error: Could not open video source.")
    exit()

# Set desired FPS
desired_fps = 30
frame_time = 1.0 / desired_fps

# Output file to store keypoints
output_file = 'keypoints.txt'
with open(output_file, 'w') as f:
    f.write("Frame\tKeypoint\tX\tY\n")  # Write header

while True:
    start_time = time.time()

    # Capture frame-by-frame
    ret, frame = cap.read()
    if not ret:
        break

    # Run YOLO model on the frame
    results = model(frame)

    # Assuming the results[0] contains the annotated frame
    annotated_frame = results[0].plot()  # plot the results on the frame

    # Extract keypoints and write to file
    keypoints = results[0].keypoints.xy
    frame_number = int(cap.get(cv2.CAP_PROP_POS_FRAMES))

    with open(output_file, 'a') as f:
        for i, keypoint in enumerate(keypoints):
            # Write frame number, keypoint index, x, y coordinates
            f.write(f"{keypoints}\n")

    # Display the frame
    cv2.imshow('YOLO Detection', annotated_frame)

    # Calculate the time taken to process the frame
    end_time = time.time()
    elapsed_time = end_time - start_time

    # Sleep to maintain the desired FPS
    time_to_sleep = frame_time - elapsed_time
    if time_to_sleep > 0:
        time.sleep(time_to_sleep)

    # Break the loop if 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release the video capture object and close all OpenCV windows
cap.release()
cv2.destroyAllWindows()
