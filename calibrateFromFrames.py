import glob
import cv2
import numpy as np

# Parameter des Schachbrett
chessboard_size = (6, 4)
# Anzahl der inneren Ecken in beiden Richtungen
square_size = 0.0355  # meter
# 3D-Punkte in der Weltkoordinateneben
objp = np.zeros((chessboard_size[0] * chessboard_size[1], 3), np.float32)
objp[:, :2] = np.mgrid[0:chessboard_size[0], 0:chessboard_size[1]].T.reshape(-1, 2)
objp *= square_size
# Arrays zum Speichern der 3D-Punkte und der 2D-Bildpunkte
objpoints = []  # 3D-Punkte im Weltkoordinatensystem
imgpoints = []  # 2D-Punkte im Bildkoordinatensystem

# Videoquelle öffnen (Webcam)
images = [cv2.imread(file) for file in glob.glob("calibFrames/*.png")]

gray = None
for image in images:
    # Schachbrett-Ecken finden
    ret, corners = cv2.findChessboardCorners(image, chessboard_size, None)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    if ret:
        # Ecken in den Bildpunkten speichern
        objpoints.append(objp)
        imgpoints.append(corners)
        # Ecken auf dem Frame zeichnen
        image = cv2.drawChessboardCorners(image, chessboard_size, corners, ret)
        # Zeige das Live-Video mit den gezeichneten Ecken
        cv2.imshow('Calibration', image)
        cv2.waitKey(0)

# Kamera-Kalibrierung durchführen
ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

# Intrinsische Kameramatrix K
print("Kameramatrix K:\n", mtx)

# Verzerrungskoeffizienten (falls benötigt)
print("Verzerrungskoeffizienten:\n", dist)
