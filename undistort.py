import cv2
import numpy as np

cap = cv2.VideoCapture("output7.mp4")  # '0' für die erste Webcam

count = 0
while True:
    ret, img = cap.read()
    if not ret:
        break

    mtx = np.array([
        [670.84500123, 0., 958.33741765],
        [0., 675.08780047, 548.98233789],
        [0., 0., 1.]
    ])

    dist = np.array([[-0.18668421, 0.01895598, -0.01765834, -0.00439377, 0.00224035]])

    dst = cv2.undistort(img, mtx, dist, None, None)

    cv2.imwrite(f'undist/{count:03d}dst.png', dst)
    cv2.imwrite(f'undist/{count:03d}img.png', img)
    count = count + 1
    #cv2.imshow("Undistorted Image", dst)
    #cv2.setWindowProperty('Undistorted Image', 1, cv2.WINDOW_NORMAL)
    #cv2.imshow("distorted image", img)
    #cv2.setWindowProperty('distorted image', 1, cv2.WINDOW_NORMAL)
    #cv2.waitKey(0)

cap.release()
cv2.destroyAllWindows()
