import imageio

images = []
rang = 308
for angle in range(0, rang):
    filename = f'/home/e321/Documents/MarsMotionLab/marcoEigen/pythonProject4/frames_3d_plot/frame_{angle:03d}.png'
    images.append(imageio.imread(filename))
imageio.mimsave('/home/e321/Documents/MarsMotionLab/marcoEigen/pythonProject4/frames_3d_plot/3d_plot_rotating.gif', images)

images = []
for angle in range(0, rang):
    filename = f'/home/e321/Documents/MarsMotionLab/marcoEigen/pythonProject4/frames_detection_1/color{angle:03d}.png'
    images.append(imageio.imread(filename))
imageio.mimsave('/home/e321/Documents/MarsMotionLab/marcoEigen/pythonProject4/frames_detection_1/3d_plot_rotating.gif', images)

images = []
for angle in range(0, rang):
    filename = f'/home/e321/Documents/MarsMotionLab/marcoEigen/pythonProject4/frames_detection_2/color{angle:03d}.png'
    images.append(imageio.imread(filename))
imageio.mimsave('/home/e321/Documents/MarsMotionLab/marcoEigen/pythonProject4/frames_detection_2/3d_plot_rotating.gif', images)

print('Animierte GIFs wurden erstellt :)')