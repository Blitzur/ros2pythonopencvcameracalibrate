import matplotlib.pyplot as plt
import numpy as np
import imageio
from sklearn.decomposition import PCA

points3D = np.array([
    [2.2316515, 0.27379286, 2.8273578],
    [2.2907453, 0.21989982, 2.7991898],
    [2.1653552, 0.20051748, 2.811385],
    [2.3204987, 0.25100785, 2.7066104],
    [2.006496, 0.20012158, 2.7156057],
    [2.3138168, 0.61342263, 2.642869],
    [1.793947, 0.5344266, 2.6255999],
    [2.4489775, 1.0703628, 2.6611505],
    [1.5705678, 0.93855906, 2.5780113],
    [2.5770307, 1.3754654, 2.6209192],
    [1.587614, 1.4222041, 2.7470913],
    [2.1623921, 1.5330374, 2.67043],
    [1.7994162, 1.4817607, 2.6590872],
    [2.1329997, 2.164723, 2.6113968],
    [1.6075386, 2.1757872, 2.6762073],
    [2.0032034, 2.611285, 2.4801767],
    [1.3826663, 2.698051, 2.5662851]
])

pca = PCA(n_components=3)
points3D = pca.fit_transform(points3D)


def rotate_x(points, angle_degrees):
    angle_rad = np.radians(angle_degrees)
    rotation_matrix = np.array([
        [1, 0, 0],
        [0, np.cos(angle_rad), -np.sin(angle_rad)],
        [0, np.sin(angle_rad), np.cos(angle_rad)]
    ])
    rotated_points = np.dot(points, rotation_matrix.T)
    return rotated_points


def rotate_y(points, angle_degrees):
    angle_radians = np.radians(angle_degrees)
    cos_theta = np.cos(angle_radians)
    sin_theta = np.sin(angle_radians)
    rotation_matrix = np.array([
        [cos_theta, 0, sin_theta],
        [0, 1, 0],
        [-sin_theta, 0, cos_theta]
    ])
    return np.dot(points, rotation_matrix)


# 3D-Punkte um 90 Grad um die Y-Achse drehen
points3D = rotate_y(points3D, 270)


def set_axes_equal():
    """ Festlegt, dass alle Achsen im 3D-Plot gleich skaliert sind """
    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    max_range = np.array(
        [x_limits[1] - x_limits[0], y_limits[1] - y_limits[0], z_limits[1] - z_limits[0]]).max() / 2.0

    mid_x = np.mean(x_limits)
    mid_y = np.mean(y_limits)
    mid_z = np.mean(z_limits)
    ax.set_xlim3d([mid_x - max_range, mid_x + max_range])
    ax.set_ylim3d([mid_y - max_range, mid_y + max_range])
    ax.set_zlim3d([mid_z - max_range, mid_z + max_range])


custom_colors = [
    '#FF0000', '#FF0000', '#FF0000',  # augen und nase
    '#00FF00', '#00FF00',  # ohren
    '#FF0000', '#FF0000',  # schultern
    '#00FFFF', '#00FFFF',  # elenbogen
    '#ff00FF', '#ff00FF',  # hände
    '#0000FF', '#0000FF',  # hüfte
    '#0000FF', '#0000FF',  # knie
    '#0000FF', '#0000FF'  # fußgelenk
]

for angle2 in range(0, 360):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(points3D[:, 0], points3D[:, 1], points3D[:, 2], c=custom_colors)
    ax.view_init(elev=20, azim=angle2)
    ax.set_title('3D Plot der Punkte')
    ax.set_xlabel('X-Achse')
    ax.set_ylabel('Y-Achse')
    ax.set_zlabel('Z-Achse')
    ax.plot(  # augen nase
        [points3D[0, 0], points3D[1, 0]],
        [points3D[0, 1], points3D[1, 1]],
        [points3D[0, 2], points3D[1, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # augen nase
        [points3D[2, 0], points3D[1, 0]],
        [points3D[2, 1], points3D[1, 1]],
        [points3D[2, 2], points3D[1, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # augen nase
        [points3D[2, 0], points3D[0, 0]],
        [points3D[2, 1], points3D[0, 1]],
        [points3D[2, 2], points3D[0, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # ohren an schultern
        [points3D[3, 0], points3D[5, 0]],
        [points3D[3, 1], points3D[5, 1]],
        [points3D[3, 2], points3D[5, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # ohren an schultern
        [points3D[4, 0], points3D[6, 0]],
        [points3D[4, 1], points3D[6, 1]],
        [points3D[4, 2], points3D[6, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # schulter schulter
        [points3D[6, 0], points3D[5, 0]],
        [points3D[6, 1], points3D[5, 1]],
        [points3D[6, 2], points3D[5, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # schulter ellen
        [points3D[6, 0], points3D[8, 0]],
        [points3D[6, 1], points3D[8, 1]],
        [points3D[6, 2], points3D[8, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # schulter elle
        [points3D[5, 0], points3D[7, 0]],
        [points3D[5, 1], points3D[7, 1]],
        [points3D[5, 2], points3D[7, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # ellen hand
        [points3D[7, 0], points3D[9, 0]],
        [points3D[7, 1], points3D[9, 1]],
        [points3D[7, 2], points3D[9, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # elle hand
        [points3D[8, 0], points3D[10, 0]],
        [points3D[8, 1], points3D[10, 1]],
        [points3D[8, 2], points3D[10, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # schulter hüfte
        [points3D[5, 0], points3D[11, 0]],
        [points3D[5, 1], points3D[11, 1]],
        [points3D[5, 2], points3D[11, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # schulter hüfte
        [points3D[6, 0], points3D[12, 0]],
        [points3D[6, 1], points3D[12, 1]],
        [points3D[6, 2], points3D[12, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # hüfte hüfte
        [points3D[11, 0], points3D[12, 0]],
        [points3D[11, 1], points3D[12, 1]],
        [points3D[11, 2], points3D[12, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # hüfte knie
        [points3D[13, 0], points3D[11, 0]],
        [points3D[13, 1], points3D[11, 1]],
        [points3D[13, 2], points3D[11, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # hüfte knie
        [points3D[14, 0], points3D[12, 0]],
        [points3D[14, 1], points3D[12, 1]],
        [points3D[14, 2], points3D[12, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # knie fuß
        [points3D[13, 0], points3D[15, 0]],
        [points3D[13, 1], points3D[15, 1]],
        [points3D[13, 2], points3D[15, 2]],
        color='gray'  # Farbe der Linie
    )
    ax.plot(  # knie fuß
        [points3D[14, 0], points3D[16, 0]],
        [points3D[14, 1], points3D[16, 1]],
        [points3D[14, 2], points3D[16, 2]],
        color='gray'  # Farbe der Linie
    )
    set_axes_equal()
    plt.savefig(
        f'frames_3d_plot/frame_{angle2:03d}.png')  # Speichere jeden Frame als PNG-Bild
    plt.close()

images = []
for i in range(0, 360):
    filename = f'frames_3d_plot/frame_{i:03d}.png'
    images.append(imageio.imread(filename))
imageio.mimsave('frames_3d_plot/3d_plot_rotating.gif',
                images)

print('Animiertes GIF wurde erstellt: 3d_plot_rotating.gif')
exit()
