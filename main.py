# ros stuff
import rclpy
from rclpy.node import Node
from std_msgs.msg import Header
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point
from sensor_msgs.msg import PointCloud2, PointField
import sensor_msgs_py.point_cloud2 as pc2

# open CV stuff
import cv2
import numpy as np
import time
import torch
from torch._inductor.triton_heuristics import foreach
from ultralytics import YOLO
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import imageio
from sklearn.decomposition import PCA

# Load the YOLO model
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = YOLO('yolov8n-pose.pt')

undistortMode = False

# Open the video source (0 for webcam, or provide a video file path)
video_source1 = "human4.mp4"  # "/dev/video4"
video_source2 = "human5.mp4"  # "/dev/video10"
cap1 = cv2.VideoCapture(video_source1)
cap2 = cv2.VideoCapture(video_source2)

# Check if video source opened successfully
if not cap1.isOpened() or not cap2.isOpened():
    print("Error: Could not open video source.")
    exit()

# Set desired FPS
desired_fps = 30
frame_time = 1.0 / desired_fps

# origin points. 3 points that ALL cameras can see
undist4originpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
undist5originpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
undist6originpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
undist7originpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo

distorigin4 = np.array([[1088, 795], [1057, 776], [1006, 816]])  # todo check if box in the middle?
distorigin5 = np.array([[1047, 830], [1023, 851], [1006, 816]])  # todo check if box in the middle?
distorigin6 = np.array([[0, 0], [0, 0], [0, 0]])  # todo
distorigin7 = np.array([[0, 0], [0, 0], [0, 0]])  # todo

dist4_45_refpoints = np.array([[1500, 566], [1465, 556], [1419, 541], [1290, 513], [1258, 502], [1299, 425]], dtype=np.float32)
dist5_45_refpoints = np.array([[865, 585], [836, 592], [798, 600], [690, 631], [642, 639], [561, 561]], dtype=np.float32)

dist5_56_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
dist6_56_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo

dist6_67_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
dist7_67_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo

dist7_74_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
dist4_74_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo

dist6_64_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
dist4_64_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo

dist7_75_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo
dist5_75_refpoints = np.array([[0.0, 0.0]], dtype=np.float32)  # todo

global_count = 0

mtx = np.array([[670.84500123, 0., 958.33741765], [0., 675.08780047, 548.98233789], [0., 0., 1.]])
mtxK4 = np.array([[1.06273552e+03, 0., 9.58672062e+02], [0., 1.18756297e+03, 5.44264218e+02], [0., 0., 1.]])
mtxK5 = np.array([[1.06273552e+03, 0., 9.58672062e+02], [0., 1.18756297e+03, 5.44264218e+02], [0., 0., 1.]])
mtxK6 = np.array([[1.06273552e+03, 0., 9.58672062e+02], [0., 1.18756297e+03, 5.44264218e+02], [0., 0., 1.]])
mtxK7 = np.array([[1.06273552e+03, 0., 9.58672062e+02], [0., 1.18756297e+03, 5.44264218e+02], [0., 0., 1.]])

# Verzerrungskoeffizienten:
dist4 = np.array([[-0.12990064, -0.35795396, -0.05435529, 0.00923628, 0.32276462]])
dist = np.array([[-0.18668421, 0.01895598, -0.01765834, -0.00439377, 0.00224035]])

mtx2 = np.array([[1.0, 0.0, 1.0], [0.0, 1.0, 1.0], [0.0, 0.0, 1.0]])  # todo correct name and is this correct like this?

Rr = np.array([[0.99146, 0.063475, -0.1139],
               [-0.067961, 0.99704, -0.035938],
               [0.11128, 0.043372, 0.99284]])
tt = np.array([[-0.96251], [0.057667], [-0.26504]])


def convert3d():
    # Essentielle Matrix berechnen
    if undistortMode:
        E, mask = cv2.findEssentialMat(points1_, points2_, mtx2, method=cv2.RANSAC, prob=0.999, threshold=1.0)
    else:
        E, mask = cv2.findEssentialMat(points1_, points2_, mtx, method=cv2.RANSAC, prob=0.999, threshold=1.0)

    # Extrahiere R und t aus der Essentiellen Matrix
    if undistortMode:
        _, R, t, mask = cv2.recoverPose(E, points1_, points2_, mtx)
    else:
        _, R, t, mask = cv2.recoverPose(E, points1_, points2_, mtx)

    #with open('Rt.txt', 'a') as file:
    #    for row in R:
    #        row_str = ', '.join(map(str, row))
    #        file.write(row_str + ', ')
    #    file.write('\n')
    #    for row in t:
    #        row_str = ', '.join(map(str, row))
    #        file.write(row_str + ', ')
    #    file.write('\n')
    print("R:")
    print(R)
    print("t:")
    print(t)

    #R = [[0.99146, 0.063475, -0.1139],
    #     [-0.067961, 0.99704, -0.035938],
    #     [0.11128, 0.043372, 0.99284]]
    #t = [[-0.96251], [0.057667], [-0.26504]]

    points1__ = np.append(distorigin4, points1_, axis=0)
    points2__ = np.append(distorigin5, points2_, axis=0)

    #points1__ = distcam4originpoints
    #points2__ = distcam5originpoints

    # Projektion der Punkte in homogene Koordinaten
    points1_hom = cv2.convertPointsToHomogeneous(points1__).reshape(-1, 3).T
    points2_hom = cv2.convertPointsToHomogeneous(points2__).reshape(-1, 3).T

    # Triangulation der 3D-Punkte
    P1 = np.dot(mtx, np.hstack((np.eye(3), np.zeros((3, 1)))))  # Kamera 1 Projektion: K1[I | 0]
    P2 = np.dot(mtx, np.hstack((R, t)))  # Kamera 2 Projektion: K2[R | t]

    points4D = cv2.triangulatePoints(P1, P2, points1_hom[:2], points2_hom[:2])
    points3D = points4D / points4D[3]

    print("3D-Punkte mit Kalibrierung:")
    points3D = points3D[:3].T

    for row in points3D:
        string = "["
        for element in row:
            string = string + str(element) + ','
        string = string[:-1] + '],'
        print(string)

    #pca = PCA(n_components=3)
    #points3D = pca.fit_transform(points3D)

    def rotate_y(points, angle_degrees):
        angle_radians = np.radians(angle_degrees)
        cos_theta = np.cos(angle_radians)
        sin_theta = np.sin(angle_radians)
        rotation_matrix = np.array([
            [cos_theta, 0, sin_theta],
            [0, 1, 0],
            [-sin_theta, 0, cos_theta]
        ])
        return np.dot(points, rotation_matrix)

    # 3D-Punkte um 90 Grad um die Y-Achse drehen
    #points3D = rotate_y(points3D, 270)

    def set_axes_equal():
        """ Festlegt, dass alle Achsen im 3D-Plot gleich skaliert sind """
        x_limits = ax.get_xlim3d()
        y_limits = ax.get_ylim3d()
        z_limits = ax.get_zlim3d()

        max_range = np.array(
            [x_limits[1] - x_limits[0], y_limits[1] - y_limits[0], z_limits[1] - z_limits[0]]).max() / 2.0

        mid_x = np.mean(x_limits)
        mid_y = np.mean(y_limits)
        mid_z = np.mean(z_limits)
        ax.set_xlim3d([mid_x - max_range, mid_x + max_range])
        ax.set_ylim3d([mid_y - max_range, mid_y + max_range])
        ax.set_zlim3d([mid_z - max_range, mid_z + max_range])

    P0 = points3D[0]
    P1 = points3D[1]
    P2 = points3D[2]
    # Translation
    Torigin = -P0

    # Neue X-Achse
    new_x_axis = (P1 - P0) / np.linalg.norm(P1 - P0)

    # Neue Z-Achse
    new_z_axis = np.cross(P1 - P0, P2 - P0)
    new_z_axis /= np.linalg.norm(new_z_axis)

    # Neue Y-Achse
    new_y_axis = np.cross(new_z_axis, new_x_axis)

    # Rotationsmatrix
    Rorigin = np.vstack([new_x_axis, new_y_axis, new_z_axis]).T

    temp = Rorigin @ (points3D.T + Torigin[:, np.newaxis])
    points3Dorigin = temp.T

    fig = plt.figure()
    ax = fig.add_subplot(122, projection='3d')
    ax2 = fig.add_subplot(121, projection='3d')
    if len(points1_) == len(points1) and len(points2_) == len(points2) and len(points3D) == 17:
        custom_colors = [
            '#FF0000', '#FF0000', '#FF0000',  # augen und nase
            '#00FF00', '#00FF00',  # ohren
            '#FF0000', '#FF0000',  # schultern
            '#00FFFF', '#00FFFF',  # elenbogen
            '#ff00FF', '#ff00FF',  # hände
            '#0000FF', '#0000FF',  # hüfte
            '#0000FF', '#0000FF',  # knie
            '#0000FF', '#0000FF'  # fußgelenk
        ]
        ax.scatter(points3D[:, 0], points3D[:, 1], points3D[:, 2], c=custom_colors)
        ax.plot(  # augen nase
            [points3D[0, 0], points3D[1, 0]],
            [points3D[0, 1], points3D[1, 1]],
            [points3D[0, 2], points3D[1, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # augen nase
            [points3D[2, 0], points3D[1, 0]],
            [points3D[2, 1], points3D[1, 1]],
            [points3D[2, 2], points3D[1, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # augen nase
            [points3D[2, 0], points3D[0, 0]],
            [points3D[2, 1], points3D[0, 1]],
            [points3D[2, 2], points3D[0, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # ohren an schultern
            [points3D[3, 0], points3D[5, 0]],
            [points3D[3, 1], points3D[5, 1]],
            [points3D[3, 2], points3D[5, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # ohren an schultern
            [points3D[4, 0], points3D[6, 0]],
            [points3D[4, 1], points3D[6, 1]],
            [points3D[4, 2], points3D[6, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # schulter schulter
            [points3D[6, 0], points3D[5, 0]],
            [points3D[6, 1], points3D[5, 1]],
            [points3D[6, 2], points3D[5, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # schulter ellen
            [points3D[6, 0], points3D[8, 0]],
            [points3D[6, 1], points3D[8, 1]],
            [points3D[6, 2], points3D[8, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # schulter elle
            [points3D[5, 0], points3D[7, 0]],
            [points3D[5, 1], points3D[7, 1]],
            [points3D[5, 2], points3D[7, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # ellen hand
            [points3D[7, 0], points3D[9, 0]],
            [points3D[7, 1], points3D[9, 1]],
            [points3D[7, 2], points3D[9, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # elle hand
            [points3D[8, 0], points3D[10, 0]],
            [points3D[8, 1], points3D[10, 1]],
            [points3D[8, 2], points3D[10, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # schulter hüfte
            [points3D[5, 0], points3D[11, 0]],
            [points3D[5, 1], points3D[11, 1]],
            [points3D[5, 2], points3D[11, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # schulter hüfte
            [points3D[6, 0], points3D[12, 0]],
            [points3D[6, 1], points3D[12, 1]],
            [points3D[6, 2], points3D[12, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # hüfte hüfte
            [points3D[11, 0], points3D[12, 0]],
            [points3D[11, 1], points3D[12, 1]],
            [points3D[11, 2], points3D[12, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # hüfte knie
            [points3D[13, 0], points3D[11, 0]],
            [points3D[13, 1], points3D[11, 1]],
            [points3D[13, 2], points3D[11, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # hüfte knie
            [points3D[14, 0], points3D[12, 0]],
            [points3D[14, 1], points3D[12, 1]],
            [points3D[14, 2], points3D[12, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # knie fuß
            [points3D[13, 0], points3D[15, 0]],
            [points3D[13, 1], points3D[15, 1]],
            [points3D[13, 2], points3D[15, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # knie fuß
            [points3D[14, 0], points3D[16, 0]],
            [points3D[14, 1], points3D[16, 1]],
            [points3D[14, 2], points3D[16, 2]],
            color='gray'  # Farbe der Linie
        )
    elif len(points3D) == 4:
        custom_colors = [
            '#FF0000', '#00FF00', '#00FFFF', '#0000FF',
        ]
        ax.scatter(points3D[:, 0], points3D[:, 1], points3D[:, 2], c=custom_colors)
        ax.plot(
            [points3D[0, 0], points3D[1, 0]],
            [points3D[0, 1], points3D[1, 1]],
            [points3D[0, 2], points3D[1, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # augen nase
            [points3D[2, 0], points3D[1, 0]],
            [points3D[2, 1], points3D[1, 1]],
            [points3D[2, 2], points3D[1, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # augen nase
            [points3D[2, 0], points3D[3, 0]],
            [points3D[2, 1], points3D[3, 1]],
            [points3D[2, 2], points3D[3, 2]],
            color='gray'  # Farbe der Linie
        )
        ax.plot(  # augen nase
            [points3D[0, 0], points3D[3, 0]],
            [points3D[0, 1], points3D[3, 1]],
            [points3D[0, 2], points3D[3, 2]],
            color='gray'  # Farbe der Linie
        )
    else:
        ax.scatter(points3D[:, 0], points3D[:, 1], points3D[:, 2])
    ax.view_init(elev=20, azim=global_count)
    ax.set_title('Origial')
    ax.set_xlabel('X-Achse')
    ax.set_ylabel('Y-Achse')
    ax.set_zlabel('Z-Achse')
    #set_axes_equal()
    ax.set_xlim3d([-2, 2])
    ax.set_ylim3d([-2, 2])
    ax.set_zlim3d([0, 2])
    plt.savefig(f'frames_3d_plot/frame_{global_count:03d}.png')  # Speichere jeden Frame als PNG-Bild
    plt.savefig(f'frames_3d_plot/frame.png')  # Speichere jeden Frame als PNG-Bild
    ax2.scatter(points3Dorigin[:, 0], points3Dorigin[:, 1], points3Dorigin[:, 2], c='r')
    ax2.view_init(elev=20, azim=global_count)
    ax2.set_title('trans')
    ax2.set_xlabel('X-Achse')
    ax2.set_ylabel('Y-Achse')
    ax2.set_zlabel('Z-Achse')
    # set_axes_equal()
    ax2.set_xlim3d([-2, 2])
    ax2.set_ylim3d([-2, 2])
    ax2.set_zlim3d([0, 2])
    plt.savefig(f'frames_3d_plot/oriframe_{global_count:03d}.png')  # Speichere jeden Frame als PNG-Bild
    plt.savefig(f'frames_3d_plot/oriframe.png')  # Speichere jeden Frame als PNG-Bild
    plt.close()


while True:
    start_time = time.time()

    # Capture frame-by-frame
    ret1, frame1 = cap1.read()
    ret2, frame2 = cap2.read()
    if not ret1 or not ret2:
        break

    #undst1 = cv2.undistort(frame1, mtx, dist, None, None)
    #undst2 = cv2.undistort(frame2, mtx, dist, None, None)

    # Run YOLO model on the frame
    #results1 = model(undst1)
    #results2 = model(undst2)
    results1 = model(frame1)
    results2 = model(frame2)

    cv2.imwrite(f'detect1.png', results1[0].plot())
    cv2.imwrite(f'detect2.png', results2[0].plot())

    if len(results1[0].boxes) == 1 and len(results2[0].boxes) == 1:
        # gpu
        resultsgpu1 = results1[0].cpu()
        resultsgpu2 = results2[0].cpu()

        # Assuming the results[0] contains the annotated frame
        # gpu
        annotated_frame1 = resultsgpu1.plot()  # plot the results on the frame
        annotated_frame2 = resultsgpu2.plot()  # plot the results on the frame

        # if cpu
        #annotated_frame1 = results1[0].plot()  # plot the results on the frame
        #annotated_frame2 = results2[0].plot()  # plot the results on the frame

        # gpu
        keypoints1 = resultsgpu1.keypoints.xy
        keypoints2 = resultsgpu2.keypoints.xy

        # cpu
        #keypoints1 = results1[0].keypoints.xy
        #keypoints2 = results2[0].keypoints.xy

        if keypoints1 is not None and keypoints1.shape[0] > 0:
            if len(keypoints1) == len(keypoints2) and len(keypoints1 == 1):
                # In numpy arrays umwandeln
                # gpu
                points1 = np.array(keypoints1[0], dtype=np.float32)
                points2 = np.array(keypoints2[0], dtype=np.float32)

                # cpu
                #points1 = np.array(keypoints1[0], dtype=np.float32)
                #points2 = np.array(keypoints2[0], dtype=np.float32)

                print(points1)
                print(points2)

                valid_indices = [(x != [0, 0]).all() and (y != [0, 0]).all() for x, y in zip(points1, points2)]

                # Filtere die Listen
                points1_ = points1[valid_indices]
                points2_ = points2[valid_indices]

                print("gefiltert:")
                print(points1_)
                print(points2_)

                if True:  # len(points1_) == len(points1) and len(points2_) == len(points2):
                    # Erstelle den 2D-Plot
                    plt.figure()
                    plt.scatter(points1_[:, 0], points1_[:, 1])
                    plt.title('2D Plot der Punkte')
                    plt.xlabel('X-Achse')
                    plt.ylabel('Y-Achse')

                    # Speichere den Plot als Bilddatei (z.B. PNG)
                    #plt.savefig('2d_plot1.png')
                    plt.cla()
                    plt.scatter(points2_[:, 0], points2_[:, 1])
                    #plt.savefig('2d_plot2.png')
                    plt.close()

                    # cv2.imshow('YOLO Detection1', annotated_frame1)
                    # cv2.imshow('YOLO Detection2', annotated_frame2)

                    convert3d()
                    cv2.imwrite(f'frames_detection_1/color{global_count:03d}.png', annotated_frame1)
                    cv2.imwrite(f'frames_detection_2/color{global_count:03d}.png', annotated_frame2)
                    global_count = global_count + 1


            else:
                print("Die Anzahl der Keypoints stimmt nicht überein.")
        else:
            raise ValueError("Ein oder beide Keypoints-Arrays sind leer.")

        # Display the frame

        # Calculate the time taken to process the frame
        end_time = time.time()
        elapsed_time = end_time - start_time

        # Sleep to maintain the desired FPS
        time_to_sleep = frame_time - elapsed_time
        if time_to_sleep > 0:
            time.sleep(time_to_sleep)

        # Break the loop if 'q' is pressed
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

# Release the video capture object and close all OpenCV windows
cap1.release()
cap2.release()
cv2.destroyAllWindows()
