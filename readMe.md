1. Record a Video of a calibration board.
2. Use VideoToCalibFrames in order to cut the whole video into single pictures, where you can see how to the calibration board was detected
3. Delete all the Pictures, where the board was detected poorly.
4. Delete all the pictures that have color, only let the gray pictures there, that had good calibrations
5. Use calibrateFromFrames.py to get the K matrix for your Camera.
6. use pickPoints to click at least 6 pixels in cam/video1 and cam/video2 that can be seen in both frames.
PS: You are not allowed to click and drag the frame, as it will detect every click as click
The clicked points will be printed in the console.
Put them in calcRt.py AND rosYolo3D.py. 
calcRt.py will use them as reference points.
rosYolo3D.py will use them to correctly rotate and translate the resulting 3D Points to origin
7. Currently, calcRt.py only supports one K matrix for both cameras, so you have to use the same hardware, 
and it probably will be little less accurate
8. Put R and t data into rosYolo3D.py to start generating 3D yolo human poses.
Currently only supports 2 Cameras

TODO:
- Calculate R and t with own K matrix for each camera
- Clean up code, currently is quite messy
- Make version without ROS visualization for PC without ROS
- Make 3D point program compatible with multiple cameras
-> You can define different camera pairs. Each par needs R and t.
