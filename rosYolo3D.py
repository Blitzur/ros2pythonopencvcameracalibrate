# ros stuff
import cv2
import rclpy
from geometry_msgs.msg import Point
from matplotlib import pyplot as plt
from rclpy.node import Node
from std_msgs.msg import Header
from visualization_msgs.msg import Marker
from sensor_msgs.msg import PointCloud2, PointField
import sensor_msgs_py.point_cloud2 as pc2

# open CV stuff
import cv2
import numpy as np
import time
import torch
from ultralytics import YOLO

video_source1 = "human5.mp4"  # "/dev/video4"
video_source2 = "human6.mp4"  # "/dev/video10"

# Load the YOLO model
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = YOLO('yolov8n-pose.pt')

# Set desired FPS
desired_fps = 25
frame_time = 1.0 / desired_fps

# Kalibrierungsdaten von OpenCV (calcRt.py)
#R45 = np.array([[-0.16299702, 0.34712577, -0.92354517], [-0.25376075, 0.88982281, 0.37923719], [0.95343456, 0.29617405, -0.05695158]], dtype=np.float32)
#t45 = np.array([[0.69496035], [-0.09293747], [0.71301665]], dtype=np.float32)

#R56 = np.array([[0.10202337, 0.00616194, -0.99476292], [-0.51202085, 0.85767522, -0.04720034], [0.85289265, 0.51415489, 0.09065796]], dtype=np.float32)
#t56 = np.array([[0.7805161], [-0.14153878], [0.60890179]], dtype=np.float32)

#R67 = np.array([[-0.01374573, 0.41535245, -0.9095567], [-0.89537719, -0.41002983, -0.17371021], [-0.44509634, 0.81200855, 0.37753327]], dtype=np.float32)
#t67 = np.array([[0.66640175], [0.36016442], [0.65283252]], dtype=np.float32)

R = np.array([[0.10202337, 0.00616194, -0.99476292], [-0.51202085, 0.85767522, -0.04720034], [0.85289265, 0.51415489, 0.09065796]], dtype=np.float32)
t = np.array([[0.7805161], [-0.14153878], [0.60890179]], dtype=np.float32)

#Kmtx4 = np.array([[1062.73552, 0., 958.672062], [0., 1187.56297, 544.264218], [0., 0., 1.]])
Kmtx5 = np.array([[670.84500123, 0., 958.33741765], [0., 675.08780047, 548.98233789], [0., 0., 1.]])  # standard
#Kmtx6 = np.array([[893.728283, .0, 950.621436], [.0, 1009.51601, 542.572038], [.0, .0, 1.]])

#dist5 = np.array([[-0.18668421, 0.01895598, -0.01765834, -0.00439377, 0.00224035]])  # standard

#distorigin4 = np.array([[1088, 795], [1059, 776], [1125, 772]])  # box bottom right, bottom left, up right
#distorigin5 = np.array([[1046, 830], [1023, 851], [1007, 816]])  # box bottom right, bottom left, up right
#distorigin6 = np.array([[1103, 559], [1127, 576], [1072, 575]])  # box bottom right, bottom left, up right
#distorigin7 = np.array([[896, 588], [924, 576], [924, 614]])     # box bottom right, bottom left, up right


distorigin2 = np.array([[1103, 559], [1127, 576], [1072, 575]])  # box bottom right, bottom left, up right
distorigin1 = np.array([[1046, 830], [1023, 851], [1007, 816]])  # box bottom right, bottom left, up right


def rotate_z_to_x_axis(points):
    # P0 und P1 als numpy arrays vorbereiten
    p0 = points[0]
    p1 = points[1]

    # Richtungsvektor der Geraden durch P0 und P1
    v = p1 - p0

    # Einheitsvektor in Richtung der X-Achse
    ex = np.array([1, 0, 0])

    # Winkel zwischen v und ex berechnen
    cos_theta = np.dot(v, ex) / (np.linalg.norm(v) * np.linalg.norm(ex))
    theta = np.arccos(cos_theta)

    # Drehachse bestimmen (Normalenvektor zu v und ex)
    n = np.cross(v, ex)
    n /= np.linalg.norm(n)  # Einheitsvektor machen

    # Rotationsmatrix um die Achse n um den Winkel theta
    Rot = np.array([[np.cos(theta) + n[0] ** 2 * (1 - np.cos(theta)),
                   n[0] * n[1] * (1 - np.cos(theta)) - n[2] * np.sin(theta),
                   n[0] * n[2] * (1 - np.cos(theta)) + n[1] * np.sin(theta)],
                  [n[1] * n[0] * (1 - np.cos(theta)) + n[2] * np.sin(theta),
                   np.cos(theta) + n[1] ** 2 * (1 - np.cos(theta)),
                   n[1] * n[2] * (1 - np.cos(theta)) - n[0] * np.sin(theta)],
                  [n[2] * n[0] * (1 - np.cos(theta)) - n[1] * np.sin(theta),
                   n[2] * n[1] * (1 - np.cos(theta)) + n[0] * np.sin(theta),
                   np.cos(theta) + n[2] ** 2 * (1 - np.cos(theta))]])

    # Punkte auf der Ebene rotieren (falls benötigt)
    # Hier wird angenommen, dass die Punkte auf der Ebene durch P0 und P1 gegeben sind.
    # Wenn die Punkte auf einer anderen Ebene liegen, sollte der Rotationsvorgang entsprechend angepasst werden.

    pointsRotated = np.dot(points, Rot.T)

    return pointsRotated


def translate_point_cloud(points):
    translation_vector = -points[0]
    # Translate all points
    translated_points = points + translation_vector
    return translated_points


def rotate_y(points, angle_degrees):
    angle_radians = np.radians(angle_degrees)
    cos_theta = np.cos(angle_radians)
    sin_theta = np.sin(angle_radians)
    rotation_matrix = np.array([
        [cos_theta, 0, sin_theta],
        [0, 1, 0],
        [-sin_theta, 0, cos_theta]
    ])
    return np.dot(points, rotation_matrix)


def translate_to_origin(points):
    # Translate points so that point 1 is at the origin
    p1 = points[0]
    translated_points = points - p1
    return translated_points


def rotate_around_z(points, angle):
    # Rotate points around the Z axis by the given angle
    cos_angle = np.cos(angle)
    sin_angle = np.sin(angle)
    rotation_matrix = np.array([
        [cos_angle, -sin_angle, 0],
        [sin_angle, cos_angle, 0],
        [0, 0, 1]
    ])
    return points @ rotation_matrix.T


def rotate_around_y(points, angle):
    # Rotate points around the Y axis by the given angle
    cos_angle = np.cos(angle)
    sin_angle = np.sin(angle)
    rotation_matrix = np.array([
        [cos_angle, 0, sin_angle],
        [0, 1, 0],
        [-sin_angle, 0, cos_angle]
    ])
    return points @ rotation_matrix.T


def rotate_x(points, angle_degrees):
    angle_rad = np.radians(angle_degrees)
    rotation_matrix = np.array([
        [1, 0, 0],
        [0, np.cos(angle_rad), -np.sin(angle_rad)],
        [0, np.sin(angle_rad), np.cos(angle_rad)]
    ])
    rotated_points = np.dot(points, rotation_matrix.T)
    return rotated_points


def transform_points(points):
    # Schritt 1: Translation
    points = translate_to_origin(points)

    # Schritt 2: Rotation um die Z-Achse, sodass Punkt 2 auf der x-Achse liegt
    p2 = points[1]
    angle_z = -np.arctan2(p2[1], p2[0])
    points = rotate_around_z(points, angle_z)

    # Schritt 3: Rotation um die Y-Achse, sodass Punkt 3 in die XY-Ebene fällt
    p3 = points[2]
    p3_rotated = rotate_around_z(np.array([p3]), angle_z)[0]
    angle_y = np.arctan2(p3_rotated[2], p3_rotated[0])
    points = rotate_around_y(points, angle_y)

    #points = rotate_x(points, 110.0)  # 45
    points = rotate_x(points, 85.0)  # 56

    return points


class CameraPositionPublisher(Node):
    def __init__(self):
        super().__init__('camera_position_publisher')

        # Open the video source (0 for webcam, or provide a video file path)
        self.cap1 = cv2.VideoCapture(video_source1)
        self.cap2 = cv2.VideoCapture(video_source2)

        # Check if video source opened successfully
        if not self.cap1.isOpened() or not self.cap2.isOpened():
            print("Error: Could not open video source.")
            exit()

        self.points_pub = self.create_publisher(PointCloud2, 'points', 1)
        self.markers_pub = self.create_publisher(Marker, 'lines', 1)
        self.points = None
        self.global_count = 0

        self.mainloop()

    def mainloop(self):
        while True:
            start_time = time.time()

            # Capture frame-by-frame
            ret1, frame1 = self.cap1.read()
            ret2, frame2 = self.cap2.read()
            if not ret1 or not ret2:
                break

            # Run YOLO model on the dist frame
            distResults1 = model(frame1)
            distResults2 = model(frame2)

            #undst1 = cv2.undistort(frame1, Kmtx5, dist5, None, None)
            #undst2 = cv2.undistort(frame2, Kmtx5, dist5, None, None)

            # Run YOLO model on the dist frame
            #undResults1 = model(undst1)
            #undResults2 = model(undst2)

            #cv2.imwrite(f'frames_detection_1/detect1.png', results1[0].plot())
            #cv2.imwrite(f'frames_detection_2/detect2.png', results2[0].plot())

            if len(distResults1[0].boxes) >= 1 and len(distResults2[0].boxes) >= 1:
                # gpu
                resultsgpu1 = distResults1[0].cpu()
                resultsgpu2 = distResults2[0].cpu()

                # Assuming the results[0] contains the annotated frame
                # gpu
                annotated_frame1 = resultsgpu1.plot()  # plot the results on the frame
                annotated_frame2 = resultsgpu2.plot()  # plot the results on the frame

                # if cpu
                # annotated_frame1 = results1[0].plot()  # plot the results on the frame
                # annotated_frame2 = results2[0].plot()  # plot the results on the frame

                # gpu
                keypoints1 = resultsgpu1.keypoints.xy
                keypoints2 = resultsgpu2.keypoints.xy

                # cpu
                # keypoints1 = results1[0].keypoints.xy
                # keypoints2 = results2[0].keypoints.xy

                if keypoints1 is not None and keypoints1.shape[0] > 0:

                    # In numpy arrays umwandeln
                    allPoints1 = np.array(keypoints1[0], dtype=np.float32)
                    allPoints2 = np.array(keypoints2[0], dtype=np.float32)

                    valid_indices = [(x != [0, 0]).all() and (y != [0, 0]).all() for x, y in
                                     zip(allPoints1, allPoints2)]

                    # Filtere die Listen
                    filterPoints1 = allPoints1[valid_indices]
                    filterPoints2 = allPoints2[valid_indices]

                    # add origin points
                    filterPoints1 = np.append(distorigin1, filterPoints1, axis=0)
                    filterPoints2 = np.append(distorigin2, filterPoints2, axis=0)

                    #for each in filterPoints1:
                    #    each[1] = 1080 - each[1]

                    #for each in filterPoints2:
                    #    each[1] = 1080 - each[1]

                    #plt.figure()
                    #plt.scatter(filterPoints1[:, 0], filterPoints1[:, 1])
                    #plt.title('2D Plot der Punkte')
                    #plt.xlabel('X-Achse')
                    #plt.ylabel('Y-Achse')
                    #plt.xlim(0, 1920)
                    #plt.ylim(0, 1080)
                    # Speichere den Plot als Bilddatei (z.B. PNG)
                    #plt.savefig(f'frames_2d_1/2d_plot{self.global_count:03d}.png')
                    #plt.savefig(f'frames_2d_1/2d_plot.png')
                    #plt.close()
                    #plt.figure()
                    #plt.scatter(filterPoints2[:, 0], filterPoints2[:, 1])
                    #plt.title('2D Plot der Punkte')
                    #plt.xlabel('X-Achse')
                    #plt.ylabel('Y-Achse')
                    #plt.xlim(0, 1920)
                    #plt.ylim(0, 1080)
                    # Speichere den Plot als Bilddatei (z.B. PNG)
                    #plt.savefig(f'frames_2d_2/2d_plot{self.global_count:03d}.png')
                    #plt.savefig(f'frames_2d_2/2d_plot.png')
                    #plt.close()
                    # todo color 2D poses AND use marker instead of PC also for color!

                    self.convert3d(filterPoints1, filterPoints2, valid_indices)
                    cv2.imwrite(f'frames_detection_1/{self.global_count:03d}dist1.png', distResults1[0].plot())
                    #cv2.imwrite(f'frames_detection_1/dist1.png', distResults1[0].plot())
                    cv2.imwrite(f'frames_detection_2/{self.global_count:03d}dist2.png', distResults2[0].plot())
                    #cv2.imwrite(f'frames_detection_2/dist2.png', distResults2[0].plot())
                    #cv2.imwrite(f'frames_detection_1/{self.global_count:03d}un1.png', undResults1[0].plot())
                    #cv2.imwrite(f'frames_detection_2/{self.global_count:03d}un2.png', undResults2[0].plot())
                    self.global_count = self.global_count + 1
                else:
                    raise ValueError("Ein oder beide Keypoints-Arrays sind leer.")

                # Display the frame

                # Calculate the time taken to process the frame
                end_time = time.time()
                elapsed_time = end_time - start_time

                # Sleep to maintain the desired FPS
                time_to_sleep = frame_time - elapsed_time
                if time_to_sleep > 0:
                    time.sleep(time_to_sleep)

        # Release the video capture object and close all OpenCV windows
        self.cap1.release()
        self.cap2.release()

    def convert3d(self, filterPoints1, filterPoints2, valid_indices):
        # Projektion der Punkte in homogene Koordinaten
        points1_hom = cv2.convertPointsToHomogeneous(filterPoints1).reshape(-1, 3).T
        points2_hom = cv2.convertPointsToHomogeneous(filterPoints2).reshape(-1, 3).T

        # Triangulation der 3D-Punkte
        P1 = np.dot(Kmtx5, np.hstack((np.eye(3), np.zeros((3, 1)))))  # Kamera 1 Projektion: K1[I | 0]
        P2 = np.dot(Kmtx5, np.hstack((R, t)))  # Kamera 2 Projektion: K2[R | t]

        points4D = cv2.triangulatePoints(P1, P2, points1_hom[:2], points2_hom[:2])
        points3D = points4D / points4D[3]

        #print("3D-Punkte mit Kalibrierung:")
        points3D = points3D[:3].T

        #for row in points3D:
        #    string = "["
        #    for element in row:
        #        string = string + str(element) + ','
        #    string = string[:-1] + '],'
        #    print(string)

        #points3D = rotate_y(points3D, 90.0)

        #transformed_points = transform_points(points3D)

        ##########################################################################

        def calculate_normal_vector(_p1, _p2, _p3):
            # Berechne die Vektoren u und v
            u = np.array(_p2) - np.array(_p1)
            v = np.array(_p3) - np.array(_p1)

            # Berechne den Normalenvektor
            n = np.cross(u, v)
            n_normalized = n / np.linalg.norm(n)

            return n_normalized

        def calculate_rotation_matrix(_normal):
            # Der Zielvektor ist (0, 0, 1)
            z_axis = np.array([0, 0, 1])

            # Berechne die Drehachse
            k = np.cross(_normal, z_axis)
            k_normalized = k / np.linalg.norm(k)

            # Berechne den Drehwinkel
            cos_theta = np.dot(_normal, z_axis)
            theta = np.arccos(cos_theta)

            # Erzeuge die Kreuzproduktmatrix K
            kx, ky, kz = k_normalized
            K = np.array([[0, -kz, ky],
                          [kz, 0, -kx],
                          [-ky, kx, 0]])

            # Berechne die Rotationsmatrix
            _I = np.identity(3)
            _Rot = _I + np.sin(theta) * K + (1 - np.cos(theta)) * np.dot(K, K)

            return _Rot

        def apply_rotation_matrix(points, R_):
            rotated_points = np.dot(points, R_.T)
            return rotated_points

        # refpunkte
        p1 = points3D[0]
        p2 = points3D[1]
        p3 = points3D[2]

        points3D = np.append(points3D, np.array([[0.0, 0.0, 0.0], [0.0, 0.0, -0.1]]), axis=0)

        # Berechne den Normalenvektor der Ebene
        normal = calculate_normal_vector(p1, p2, p3)

        # Berechne die Rotationsmatrix
        Rot = calculate_rotation_matrix(normal)

        transformed_points = np.dot(points3D, Rot.T)

        transformed_points = rotate_x(transformed_points, 180.0)

        transformed_points = translate_point_cloud(transformed_points)

        transformed_points = rotate_z_to_x_axis(transformed_points)

        ##########################################################################

        for e in transformed_points:
            e[0] *= 5.0
            e[1] *= 5.0
            e[2] *= 5.0

        pc = self.create_point_cloud(transformed_points, valid_indices)
        self.points_pub.publish(pc)
        lm = self.create_line_marker(transformed_points, valid_indices)
        self.markers_pub.publish(lm)

    def create_line_marker(self, points, valid_indices):
        marker = Marker()
        marker.header.frame_id = "base_link"
        marker.header.stamp = self.get_clock().now().to_msg()
        marker.ns = "lines"
        marker.id = 0
        marker.type = Marker.LINE_LIST
        marker.action = Marker.ADD
        marker.scale.x = 0.05
        marker.color.a = 1.0
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0

        # ref board floor
        marker.points.append(Point(x=float(points[0][0]), y=float(points[0][1]), z=float(points[0][2])))
        marker.points.append(Point(x=float(points[1][0]), y=float(points[1][1]), z=float(points[1][2])))
        marker.points.append(Point(x=float(points[2][0]), y=float(points[2][1]), z=float(points[2][2])))
        marker.points.append(Point(x=float(points[1][0]), y=float(points[1][1]), z=float(points[1][2])))
        marker.points.append(Point(x=float(points[2][0]), y=float(points[2][1]), z=float(points[2][2])))
        marker.points.append(Point(x=float(points[0][0]), y=float(points[0][1]), z=float(points[0][2])))

        # kamera position an direction
        marker.points.append(Point(x=float(points[len(points)-2][0]), y=float(points[len(points)-2][1]), z=float(points[len(points)-2][2])))
        marker.points.append(Point(x=float(points[len(points)-1][0]), y=float(points[len(points)-1][1]), z=float(points[len(points)-1][2])))

        pointsWithGaps = []
        temp = 3
        for b in valid_indices:
            if b:
                pointsWithGaps.append([points[temp][0], points[temp][1], points[temp][2]])
                temp += 1
            else:
                pointsWithGaps.append([0.0, 0.0, 0.0])

        #print(pointsWithGaps)

        if valid_indices[0]:  # nase
            if valid_indices[1]:  # auf auge1
                marker.points.append(
                    Point(x=float(pointsWithGaps[0][0]), y=float(pointsWithGaps[0][1]), z=float(pointsWithGaps[0][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[1][0]), y=float(pointsWithGaps[1][1]), z=float(pointsWithGaps[1][2])))
            if valid_indices[2]:  # auf auge2
                marker.points.append(
                    Point(x=float(pointsWithGaps[0][0]), y=float(pointsWithGaps[0][1]), z=float(pointsWithGaps[0][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[2][0]), y=float(pointsWithGaps[2][1]), z=float(pointsWithGaps[2][2])))
        if valid_indices[3]:  # ohr1
            if valid_indices[5]:  # auf schulter1
                marker.points.append(
                    Point(x=float(pointsWithGaps[3][0]), y=float(pointsWithGaps[3][1]), z=float(pointsWithGaps[3][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[5][0]), y=float(pointsWithGaps[5][1]), z=float(pointsWithGaps[5][2])))
        if valid_indices[4]:  # ohr2
            if valid_indices[6]:  # auf schulter2
                marker.points.append(
                    Point(x=float(pointsWithGaps[4][0]), y=float(pointsWithGaps[4][1]), z=float(pointsWithGaps[4][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[6][0]), y=float(pointsWithGaps[6][1]), z=float(pointsWithGaps[6][2])))
        if valid_indices[5]:  # ohr2
            if valid_indices[6]:  # auf schulter2
                marker.points.append(
                    Point(x=float(pointsWithGaps[5][0]), y=float(pointsWithGaps[5][1]), z=float(pointsWithGaps[5][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[6][0]), y=float(pointsWithGaps[6][1]), z=float(pointsWithGaps[6][2])))
        if valid_indices[8]:  # ohr2
            if valid_indices[6]:  # auf schulter2
                marker.points.append(
                    Point(x=float(pointsWithGaps[8][0]), y=float(pointsWithGaps[8][1]), z=float(pointsWithGaps[8][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[6][0]), y=float(pointsWithGaps[6][1]), z=float(pointsWithGaps[6][2])))
        if valid_indices[5]:  # ohr2
            if valid_indices[7]:  # auf schulter2
                marker.points.append(
                    Point(x=float(pointsWithGaps[5][0]), y=float(pointsWithGaps[5][1]), z=float(pointsWithGaps[5][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[7][0]), y=float(pointsWithGaps[7][1]), z=float(pointsWithGaps[7][2])))
        if valid_indices[9]:  # ohr2
            if valid_indices[7]:  # auf schulter2
                marker.points.append(
                    Point(x=float(pointsWithGaps[9][0]), y=float(pointsWithGaps[9][1]), z=float(pointsWithGaps[9][2])))
                marker.points.append(
                    Point(x=float(pointsWithGaps[7][0]), y=float(pointsWithGaps[7][1]), z=float(pointsWithGaps[7][2])))
        if valid_indices[8]:  # ohr2
            if valid_indices[10]:  # auf schulter2
                marker.points.append(
                    Point(x=float(pointsWithGaps[8][0]), y=float(pointsWithGaps[8][1]), z=float(pointsWithGaps[8][2])))
                marker.points.append(Point(x=float(pointsWithGaps[10][0]), y=float(pointsWithGaps[10][1]),
                                           z=float(pointsWithGaps[10][2])))
        if valid_indices[5]:  # schulter hüfte
            if valid_indices[11]:
                marker.points.append(
                    Point(x=float(pointsWithGaps[5][0]), y=float(pointsWithGaps[5][1]), z=float(pointsWithGaps[5][2])))
                marker.points.append(Point(x=float(pointsWithGaps[11][0]), y=float(pointsWithGaps[11][1]),
                                           z=float(pointsWithGaps[11][2])))
        if valid_indices[6]:  # schulter hüfte
            if valid_indices[12]:
                marker.points.append(
                    Point(x=float(pointsWithGaps[6][0]), y=float(pointsWithGaps[6][1]), z=float(pointsWithGaps[6][2])))
                marker.points.append(Point(x=float(pointsWithGaps[12][0]), y=float(pointsWithGaps[12][1]),
                                           z=float(pointsWithGaps[12][2])))
        if valid_indices[11]:  # schulter hüfte
            if valid_indices[12]:
                marker.points.append(Point(x=float(pointsWithGaps[11][0]), y=float(pointsWithGaps[11][1]),
                                           z=float(pointsWithGaps[11][2])))
                marker.points.append(Point(x=float(pointsWithGaps[12][0]), y=float(pointsWithGaps[12][1]),
                                           z=float(pointsWithGaps[12][2])))
        if valid_indices[11]:  # schulter hüfte
            if valid_indices[13]:
                marker.points.append(Point(x=float(pointsWithGaps[11][0]), y=float(pointsWithGaps[11][1]),
                                           z=float(pointsWithGaps[11][2])))
                marker.points.append(Point(x=float(pointsWithGaps[13][0]), y=float(pointsWithGaps[13][1]),
                                           z=float(pointsWithGaps[13][2])))
        if valid_indices[14]:  # schulter hüfte
            if valid_indices[12]:
                marker.points.append(Point(x=float(pointsWithGaps[14][0]), y=float(pointsWithGaps[14][1]),
                                           z=float(pointsWithGaps[14][2])))
                marker.points.append(Point(x=float(pointsWithGaps[12][0]), y=float(pointsWithGaps[12][1]),
                                           z=float(pointsWithGaps[12][2])))
        if valid_indices[15]:  # schulter hüfte
            if valid_indices[13]:
                marker.points.append(Point(x=float(pointsWithGaps[15][0]), y=float(pointsWithGaps[15][1]),
                                           z=float(pointsWithGaps[15][2])))
                marker.points.append(Point(x=float(pointsWithGaps[13][0]), y=float(pointsWithGaps[13][1]),
                                           z=float(pointsWithGaps[13][2])))
        if valid_indices[14]:  # schulter hüfte
            if valid_indices[16]:
                marker.points.append(Point(x=float(pointsWithGaps[14][0]), y=float(pointsWithGaps[14][1]),
                                           z=float(pointsWithGaps[14][2])))
                marker.points.append(Point(x=float(pointsWithGaps[16][0]), y=float(pointsWithGaps[16][1]),
                                           z=float(pointsWithGaps[16][2])))

        return marker

    def create_point_cloud(self, points, valid_indices):
        header = Header()
        header.stamp = self.get_clock().now().to_msg()
        header.frame_id = 'base_link'

        fields = [
            PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1),
        ]

        pc = pc2.create_cloud(header, fields, points)
        return pc


def main(args=None):
    rclpy.init(args=args)
    camera_position_publisher = CameraPositionPublisher()
    rclpy.spin(camera_position_publisher)
    camera_position_publisher.destroy_node()
    rclpy.shutdown()


if True:
    main()
