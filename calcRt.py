import cv2
import numpy as np

dist445CalibPoints = np.array([[1501, 566], [1466, 557], [1418, 541], [1289, 513], [1258, 501], [1299, 439], [1299, 424], [1088, 795], [1058, 776], [1125, 771], [1094, 754]], dtype=np.float32)
dist545CalibPoints = np.array([[865, 585], [837, 592], [798, 600], [691, 631], [642, 639], [564, 575], [561, 560], [1046, 830], [1023, 851], [1007, 816], [981, 835]], dtype=np.float32)

dist556CalibPoints = np.array([[1002, 523], [1068, 522], [1073, 522], [1152, 522], [1158, 522], [1249, 523], [1010, 614], [1073, 621], [1080, 623], [1156, 632], [1162, 632], [1250, 643]], dtype=np.float32)
dist656CalibPoints = np.array([[761, 315], [857, 302], [863, 301], [947, 293], [953, 292], [1025, 286], [771, 429], [860, 410], [867, 409], [946, 393], [953, 392], [1021, 380]], dtype=np.float32)

dist664CalibPoints = np.array([[1103, 559], [1127, 576], [1072, 575], [1097, 593], [1534, 743], [1546, 701]], dtype=np.float32)
dist464CalibPoints = np.array([[1088, 796], [1057, 776], [1126, 772], [1094, 754], [855, 532], [815, 540]], dtype=np.float32)

dist667CalibPoints = np.array([], dtype=np.float32)
dist767CalibPoints = np.array([], dtype=np.float32)

dist774CalibPoints = np.array([], dtype=np.float32)
dist474CalibPoints = np.array([], dtype=np.float32)


Kmtx5 = np.array([[670.84500123, 0., 958.33741765], [0., 675.08780047, 548.98233789], [0., 0., 1.]])  # standard

# Berechnung der essentiellen Matrix
E, _ = cv2.findEssentialMat(dist664CalibPoints, dist464CalibPoints, Kmtx5, method=cv2.RANSAC, prob=0.999, threshold=1.0)

# Schätzung der Pose
_, R, t, _ = cv2.recoverPose(E, dist664CalibPoints, dist464CalibPoints, Kmtx5)

# Ausgabe der Ergebnisse
#print("Essential Matrix: \n", E)
print("Rotation Matrix: \n", R)
print("Translation Vector: \n", t)
