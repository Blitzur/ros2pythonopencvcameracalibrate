import cv2
import numpy as np

# Globale Variablen zur Speicherung der Punkte
points1 = []
current_frame = 1
points_needed = 12

cap = cv2.VideoCapture(
    "origin4.mp4")  # Verwenden Sie 0 für die Standardkamera oder geben Sie den Pfad zu einer Videodatei an


# Callback-Funktion für Mausklicks
def click_event(event, x, y, flags, params):
    global current_frame
    if event == cv2.EVENT_LBUTTONDOWN:
        points1.append((x, y))
        print(f"[{x}, {y}], ", end="")


# Laden Sie Ihr YOLOv8-Modell (falls benötigt)
# model = YOLO('yolov8.pt')  # Passen Sie den Pfad zu Ihrem Modell an


# Überprüfen Sie, ob die Kamera geöffnet wurde
if not cap.isOpened():
    print("Fehler beim Öffnen der Kamera oder Videodatei")
    exit()

# Setzen Sie die gewünschte Auflösung
cv2.namedWindow('Frame')
cv2.setMouseCallback('Frame', click_event)

while True:
    ret, frame = cap.read()
    if not ret:
        break

    mtx = np.array([
        [670.84500123, 0., 958.33741765],
        [0., 675.08780047, 548.98233789],
        [0., 0., 1.]
    ])

    dist = np.array([[-0.18668421, 0.01895598, -0.01765834, -0.00439377, 0.00224035]])

    #frame = cv2.undistort(frame, mtx, dist, None, None)

    # Anzeigen des Frames und der ausgewählten Punkte
    for point in points1:
        cv2.circle(frame, point, 1, (0, 0, 255), -1)

    cv2.imshow('Frame', frame)
    cv2.waitKey(0)

    key = cv2.waitKey(1) & 0xFF
    if key == ord('n'):
        # Überprüfen, ob genug Punkte in Frame 1 ausgewählt wurden
        if current_frame == 1 and len(points1) == points_needed:
            break
    elif key == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()

# Überprüfen, ob genug Punkte ausgewählt wurden
if len(points1) != points_needed:
    print("Es wurden nicht genügend Punkte ausgewählt.")
    exit()

# Konvertiere Punkte zu numpy Arrays
points1 = np.array(points1, dtype=np.float32)
print(points1)

# Beispielhafte Kameramatrix K (f_x, f_y, c_x, c_y)
K = np.array([
    [670.84500123, 0., 958.33741765],
    [0., 675.08780047, 548.98233789],
    [0., 0., 1.]
])

# Berechnung der essentiellen Matrix
#essentialMatrix, _ = cv2.findEssentialMat(points1, points2, K)

# Schätzung der Pose
#_, R, t, _ = cv2.recoverPose(essentialMatrix, points1, points2, K)

# Ausgabe der Ergebnisse
#print("Essential Matrix: \n", essentialMatrix)
#print("Rotation Matrix: \n", R)
#print("Translation Vector: \n", t)
